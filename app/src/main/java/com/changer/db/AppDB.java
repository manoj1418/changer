package com.changer.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.changer.dao.DataDao;
import com.changer.model.DataModel;


@Database(entities = {
        DataModel.class}, version = 1, exportSchema = false)

public abstract class AppDB extends RoomDatabase {
    private static final String DB_NAME = "Changer";
    private static AppDB appDB;

    public synchronized static AppDB getInstance(final Context ctx) {
        if (appDB == null) {
            appDB = Room.databaseBuilder(ctx, AppDB.class, DB_NAME).allowMainThreadQueries().build();
        }
        return appDB;
    }

    public abstract DataDao dataDao();
}