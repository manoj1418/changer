package com.changer.model;

import java.util.List;

public class CampaignModel {
    private String camp_id;
    private String camp_name;
    private String automation;
    private String install_type;
    private String s_url;
    private String a_url;
    private String d_url;
    private String s_package;
    private String d_path;
    private String element_type;
    private String element_class;
    private String element_id;
    private List<CommandModel> playstore_cmd;
    private List<CommandModel> url_cmd;
    private List<CommandModel> a_url_cmd;

    public String getCamp_id() {
        return camp_id;
    }

    public void setCamp_id(String camp_id) {
        this.camp_id = camp_id;
    }

    public String getCamp_name() {
        return camp_name;
    }

    public void setCamp_name(String camp_name) {
        this.camp_name = camp_name;
    }

    public String getAutomation() {
        return automation;
    }

    public void setAutomation(String automation) {
        this.automation = automation;
    }

    public String getInstall_type() {
        return install_type;
    }

    public void setInstall_type(String install_type) {
        this.install_type = install_type;
    }

    public String getS_url() {
        return s_url;
    }

    public void setS_url(String s_url) {
        this.s_url = s_url;
    }

    public String getA_url() {
        return a_url;
    }

    public void setA_url(String a_url) {
        this.a_url = a_url;
    }

    public String getD_url() {
        return d_url;
    }

    public void setD_url(String d_url) {
        this.d_url = d_url;
    }

    public String getS_package() {
        return s_package;
    }

    public void setS_package(String s_package) {
        this.s_package = s_package;
    }

    public String getD_path() {
        return d_path;
    }

    public void setD_path(String d_path) {
        this.d_path = d_path;
    }

    public String getElement_type() {
        return element_type;
    }

    public void setElement_type(String element_type) {
        this.element_type = element_type;
    }

    public String getElement_class() {
        return element_class;
    }

    public void setElement_class(String element_class) {
        this.element_class = element_class;
    }

    public String getElement_id() {
        return element_id;
    }

    public void setElement_id(String element_id) {
        this.element_id = element_id;
    }

    public List<CommandModel> getPlaystore_cmd() {
        return playstore_cmd;
    }

    public void setPlaystore_cmd(List<CommandModel> playstore_cmd) {
        this.playstore_cmd = playstore_cmd;
    }

    public List<CommandModel> getUrl_cmd() {
        return url_cmd;
    }

    public void setUrl_cmd(List<CommandModel> url_cmd) {
        this.url_cmd = url_cmd;
    }

    public List<CommandModel> getA_url_cmd() {
        return a_url_cmd;
    }

    public void setA_url_cmd(List<CommandModel> a_url_cmd) {
        this.a_url_cmd = a_url_cmd;
    }
}