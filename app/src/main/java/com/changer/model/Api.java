package com.changer.model;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Api {
    @GET("app_control.php")
    Call<List<InfoModel>> getInfo();

    @GET("app_gaccounts.php")
    Call<List<UserModel>> getUser();

    @GET("api/timezone/Asia/Kolkata")
    Call<TimeModel> getTime();

    @GET("getalldevices.php")
    Call<List<DeviceModel>> getDevice();

    @GET("getallcampaigns.php")
    Call<List<CampaignModel>> getCampaign(@Query("deviceid") String deviceid);

    @GET("readtasks.php")
    Call<CampaignModel> get_all_tasks(@Query("campaignid") String campaignid);
}