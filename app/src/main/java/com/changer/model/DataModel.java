package com.changer.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.changer.utils.Converters;

import java.util.List;

@Entity(tableName = "data")
public class DataModel {
    @PrimaryKey
    private int id;
    private String status;
    private String automation;
    private String install_type;
    private String s_url;
    private String a_url;
    private String d_url;
    private String s_package;
    private String d_path;
    private String element_type;
    private String element_class;
    private String element_id;
    private String backup;
    private String restore;
    private String no_user;
    @TypeConverters(Converters.class)
    private List<CommandModel> playstore_cmd;
    @TypeConverters(Converters.class)
    private List<CommandModel> url_cmd;
    @TypeConverters(Converters.class)
    private List<CommandModel> a_url_cmd;
    private String android_id;
    private String model;
    private String android_version;
    private String sdk;
    private String fingerprint;
    private String install_count;
    private String reboot;

    public DataModel(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAutomation() {
        return automation;
    }

    public void setAutomation(String automation) {
        this.automation = automation;
    }

    public String getInstall_type() {
        return install_type;
    }

    public void setInstall_type(String install_type) {
        this.install_type = install_type;
    }

    public String getS_url() {
        return s_url;
    }

    public void setS_url(String s_url) {
        this.s_url = s_url;
    }

    public String getD_url() {
        return d_url;
    }

    public void setD_url(String d_url) {
        this.d_url = d_url;
    }

    public String getA_url() {
        return a_url;
    }

    public void setA_url(String a_url) {
        this.a_url = a_url;
    }

    public String getS_package() {
        return s_package;
    }

    public void setS_package(String s_package) {
        this.s_package = s_package;
    }

    public String getD_path() {
        return d_path;
    }

    public void setD_path(String d_path) {
        this.d_path = d_path;
    }

    public String getElement_type() {
        return element_type;
    }

    public void setElement_type(String element_type) {
        this.element_type = element_type;
    }

    public String getElement_class() {
        return element_class;
    }

    public void setElement_class(String element_class) {
        this.element_class = element_class;
    }

    public String getElement_id() {
        return element_id;
    }

    public void setElement_id(String element_id) {
        this.element_id = element_id;
    }

    public String getBackup() {
        return backup;
    }

    public void setBackup(String backup) {
        this.backup = backup;
    }

    public String getRestore() {
        return restore;
    }

    public void setRestore(String restore) {
        this.restore = restore;
    }

    public String getNo_user() {
        return no_user;
    }

    public void setNo_user(String no_user) {
        this.no_user = no_user;
    }

    public List<CommandModel> getPlaystore_cmd() {
        return playstore_cmd;
    }

    public void setPlaystore_cmd(List<CommandModel> playstore_cmd) {
        this.playstore_cmd = playstore_cmd;
    }

    public List<CommandModel> getUrl_cmd() {
        return url_cmd;
    }

    public void setUrl_cmd(List<CommandModel> url_cmd) {
        this.url_cmd = url_cmd;
    }

    public List<CommandModel> getA_url_cmd() {
        return a_url_cmd;
    }

    public void setA_url_cmd(List<CommandModel> a_url_cmd) {
        this.a_url_cmd = a_url_cmd;
    }

    public String getAndroid_id() {
        return android_id;
    }

    public void setAndroid_id(String android_id) {
        this.android_id = android_id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getAndroid_version() {
        return android_version;
    }

    public void setAndroid_version(String android_version) {
        this.android_version = android_version;
    }

    public String getSdk() {
        return sdk;
    }

    public void setSdk(String sdk) {
        this.sdk = sdk;
    }

    public String getFingerprint() {
        return fingerprint;
    }

    public void setFingerprint(String fingerprint) {
        this.fingerprint = fingerprint;
    }

    public String getInstall_count() {
        return install_count;
    }

    public void setInstall_count(String install_count) {
        this.install_count = install_count;
    }

    public String getReboot() {
        return reboot;
    }

    public void setReboot(String reboot) {
        this.reboot = reboot;
    }
}