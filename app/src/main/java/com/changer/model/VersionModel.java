package com.changer.model;

public class VersionModel {
    private String version;
    private String sdk;

    public VersionModel(String version, String sdk) {
        this.version = version;
        this.sdk = sdk;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getSdk() {
        return sdk;
    }

    public void setSdk(String sdk) {
        this.sdk = sdk;
    }
}