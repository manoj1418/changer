package com.changer.model;

public class TimeModel {
    private String unixtime;

    public String getUnixtime() {
        return unixtime;
    }

    public void setUnixtime(String unixtime) {
        this.unixtime = unixtime;
    }
}