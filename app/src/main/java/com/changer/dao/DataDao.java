package com.changer.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.changer.model.DataModel;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface DataDao {
    @Insert(onConflict = REPLACE)
    void insert_data(DataModel dataModel);

    @Query("select count(*) from data")
    int get_data_count();

    @Query("select * from data where id = :id")
    DataModel get_data(int id);

    //Status
    @Query("select status from data where id = :id")
    String get_status(int id);

    @Query("update data set status=:status where id = :id")
    void set_status(String status, int id);

    //Automation
    @Query("select automation from data where id = :id")
    String get_automation(int id);

    @Query("update data set automation=:automation where id = :id")
    void set_automation(String automation, int id);

    //install type
    @Query("select install_type from data where id = :id")
    String get_install_type(int id);

    @Query("update data set install_type=:install_type where id = :id")
    void update_install_type(String install_type, int id);

    //store url
    @Query("update data set s_url=:s_url, a_url=:a_url, d_url=:d_url, s_package=:s_package, d_path=:d_path where id = :id")
    int update_url_type(String s_url, String a_url, String d_url, String s_package, String d_path, int id);

    @Query("update data set backup=:backup, android_id=:android_id, model=:model, android_version=:android_version, sdk=:sdk, fingerprint=:fingerprint where id = :id")
    void update_backup(String backup, String android_id, String model, String android_version, String sdk, String fingerprint, int id);

    //download path
    @Query("select d_path from data where id = :id")
    String get_download_path(int id);

    @Query("update data set d_path=:d_path where id = :id")
    void update_download_path(String d_path, int id);

    //element type
    @Query("select element_type from data where id = :id")
    String get_element_type(int id);

    @Query("update data set element_type=:element_type where id = :id")
    void update_element_type(String element_type, int id);

    //element class
    @Query("select element_class from data where id = :id")
    String get_element_class(int id);

    @Query("update data set element_class=:element_class where id = :id")
    void update_element_class(String element_class, int id);

    //element id
    @Query("select element_id from data where id = :id")
    String get_element_id(int id);

    @Query("update data set element_id=:element_id where id = :id")
    void update_element_id(String element_id, int id);

    //Playstore CMD
    @Query("select playstore_cmd from data where id = :id")
    String get_playstore_cmd(int id);

    @Query("update data set playstore_cmd=:playstore_cmd where id = :id")
    void update_playstore_cmd(String playstore_cmd, int id);

    //Url CMD
    @Query("select url_cmd from data where id = :id")
    String get_url_cmd(int id);

    @Query("update data set url_cmd=:url_cmd where id = :id")
    void update_url_cmd(String url_cmd, int id);

    //Action Url CMD
    @Query("select a_url_cmd from data where id = :id")
    String get_a_url_cmd(int id);

    @Query("update data set a_url_cmd=:a_url_cmd where id = :id")
    void update_a_url_cmd(String a_url_cmd, int id);

    //No User
    @Query("select no_user from data where id = :id")
    String get_no_user(int id);

    @Query("update data set no_user=:no_user where id = :id")
    void update_no_user(String no_user, int id);

    //install count
    @Query("select install_count from data where id = :id")
    String get_install_count(int id);

    @Query("update data set install_count=:install_count where id = :id")
    void update_install_count(String install_count, int id);

    //reboot
    @Query("select reboot from data where id = :id")
    String get_reboot(int id);

    @Query("update data set reboot=:reboot where id = :id")
    void set_reboot(String reboot, int id);

    //backup
    @Query("select backup from data where id = :id")
    String check_backup(int id);

    //update task
    @Query("update data set automation=:automation, install_type=:install_type, s_url=:s_url, a_url=:a_url, d_url=:d_url, s_package=:s_package, d_path=:d_path, element_type=:element_type, element_class=:element_class, element_id=:element_id, playstore_cmd=:playstore_cmd, url_cmd=:url_cmd, a_url_cmd=:a_url_cmd where id = :id")
    void update_task(String automation, String install_type, String s_url, String a_url, String d_url, String s_package, String d_path, String element_type, String element_class, String element_id, String playstore_cmd, String url_cmd, String a_url_cmd, int id);
}