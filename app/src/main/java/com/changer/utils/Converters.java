package com.changer.utils;

import androidx.room.TypeConverter;

import com.changer.model.CommandModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

public class Converters {
    @TypeConverter
    public List<CommandModel> set_command_model(String s_cmd) {
        if (s_cmd == null) {
            return (null);
        }
        return new Gson().fromJson(s_cmd, new TypeToken<List<CommandModel>>() {
        }.getType());
    }

    @TypeConverter
    public String get_command_model(List<CommandModel> cm) {
        if (cm == null) {
            return (null);
        }
        return new Gson().toJson(cm, new TypeToken<List<CommandModel>>() {
        }.getType());
    }
}