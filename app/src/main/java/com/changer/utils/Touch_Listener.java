package com.changer.utils;

public interface Touch_Listener {
    void Drag(int f_position, int t_position);

    void Swipe(int position);

    void Edit(int position);
}