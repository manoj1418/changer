package com.changer.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.provider.Settings;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.changer.db.AppDB;
import com.changer.model.VersionModel;
import com.github.ybq.android.spinkit.SpinKitView;
import com.google.android.material.snackbar.Snackbar;
import com.stericson.RootShell.RootShell;
import com.stericson.RootShell.execution.Command;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Properties;
import java.util.Random;

public class Utils {
    @SuppressLint("SdCardPath")
    public static String m_sdcard = "/sdcard";
    public static String pro = m_sdcard + "/propreplace.txt";
    public static String temp = m_sdcard + "/buildprop.tmp";
    public static String backup = m_sdcard + "/build.prop.bak";
    public static StringBuilder cmd_builder;
    private static int id_count = 0;

    public static boolean isEmpty(EditText etText) {
        return etText.getText().toString().trim().length() == 0;
    }

    public static void createTempFile(Context ctx) {
        if (!new File(temp).exists()) {
            cmd_builder = new StringBuilder();
            cmd_builder.append("mount -o remount,rw /system").append("\n").append("sleep 1").append("\n");
            cmd_builder.append("mount -o remount,rw rootfs").append("\n").append("sleep 1").append("\n");
            cmd_builder.append("cp -f /system/build.prop ").append(m_sdcard).append("/buildprop.tmp").append("\n").append("sleep 1").append("\n");
            cmd_builder.append("chmod 777 ").append(m_sdcard).append("/buildprop.tmp").append("\n").append("sleep 1").append("\n");
            com_exe(ctx, String.valueOf(cmd_builder));
        }
    }

    public static void backup(Context ctx, ConstraintLayout lay_cons) {
        createTempFile(ctx);
        if (!new File(backup).exists()) {
            cmd_builder = new StringBuilder();
            cmd_builder.append("mount -o remount,rw /system").append("\n").append("sleep 1").append("\n");
            cmd_builder.append("mount -o remount,rw rootfs").append("\n").append("sleep 1").append("\n");
            cmd_builder.append("cp -f /system/build.prop ").append(m_sdcard).append("/build.prop.bak").append("\n").append("sleep 1").append("\n");
            cmd_builder.append("chmod 777 ").append(m_sdcard).append("/build.prop.bak").append("\n").append("sleep 1").append("\n");
            com_exe(ctx, String.valueOf(cmd_builder));
            Snackbar.make(lay_cons, "Backup file created", Snackbar.LENGTH_SHORT).show();
        } else {
            Snackbar.make(lay_cons, "Backup file already created", Snackbar.LENGTH_SHORT).show();
        }
        if (AppDB.getInstance(ctx).dataDao().check_backup(1) == null) {
            get_prop(ctx);
        }
    }

    public static void restore(Context ctx, ConstraintLayout lay_cons) {
        if (new File(backup).exists()) {
            cmd_builder = new StringBuilder();
            cmd_builder.append("mount -o remount,rw /system").append("\n").append("sleep 1").append("\n");
            cmd_builder.append("mount -o remount,rw rootfs").append("\n").append("sleep 1").append("\n");
            cmd_builder.append("mv -f /system/build.prop /system/build.prop.bak").append("\n").append("sleep 1").append("\n");
            cmd_builder.append("mount -o remount,rw /system").append("\n").append("sleep 1").append("\n");
            cmd_builder.append("mount -o remount,rw rootfs").append("\n").append("sleep 1").append("\n");
            cmd_builder.append("cp -f ").append(m_sdcard).append("/build.prop.bak /system/build.prop").append("\n").append("sleep 1").append("\n");
            cmd_builder.append("chmod 644 /system/build.prop").append("\n").append("sleep 1").append("\n");
            com_exe(ctx, String.valueOf(cmd_builder));
            Snackbar.make(lay_cons, "Restored successfuly", Snackbar.LENGTH_SHORT).show();
        } else {
            Snackbar.make(lay_cons, "Backup file already restored or no backup file found", Snackbar.LENGTH_SHORT).show();
        }
    }

    public static void com_exe(Context ctx, String cmd) {
        if (cmd != null) {
            Command command = new Command(id_count, cmd) {
                @Override
                public void commandOutput(int id, String line) {
                    super.commandOutput(id, line);
                }

                @Override
                public void commandTerminated(int id, String reason) {
                    super.commandTerminated(id, reason);
                }

                @Override
                public void commandCompleted(int id, int exitcode) {
                    super.commandCompleted(id, exitcode);
                }
            };
            try {
                RootShell.defaultCommandTimeout = 600000;
                RootShell.getShell(true).add(command);
                id_count++;
            } catch (Exception e) {
                Toast.makeText(ctx, String.valueOf(e), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @SuppressLint("HardwareIds")
    private static void get_prop(Context ctx) {
        Properties properties = new Properties();
        try {
            String original = "/system/build.prop";
            FileInputStream in = new FileInputStream(new File(original));
            properties.load(in);
            in.close();
            AppDB.getInstance(ctx).dataDao().update_backup("1",
                    Settings.Secure.getString(ctx.getContentResolver(), "android_id"),
                    properties.getProperty("ro.product.model"),
                    properties.getProperty("ro.build.version.release"),
                    properties.getProperty("ro.build.version.sdk"),
                    properties.getProperty("ro.build.fingerprint"),
                    1);
        } catch (IOException e) {
            Toast.makeText(ctx, "Error: " + e, Toast.LENGTH_SHORT).show();
        }
    }

    public static VersionModel gen_random_version() {
        List<VersionModel> versionModels = new ArrayList<>();
        versionModels.add(new VersionModel("4.0", "14"));
        versionModels.add(new VersionModel("4.0.4", "15"));
        versionModels.add(new VersionModel("4.1", "16"));
        versionModels.add(new VersionModel("4.3.1", "18"));
        versionModels.add(new VersionModel("4.4", "19"));
        versionModels.add(new VersionModel("4.4.4", "20"));
        versionModels.add(new VersionModel("5.0", "21"));
        versionModels.add(new VersionModel("5.1.1", "22"));
        versionModels.add(new VersionModel("6.0", "23"));
        versionModels.add(new VersionModel("6.0.1", "23"));
        versionModels.add(new VersionModel("7.0", "24"));
        versionModels.add(new VersionModel("7.1.2", "25"));
        versionModels.add(new VersionModel("8.0", "26"));
        versionModels.add(new VersionModel("8.1", "27"));
        versionModels.add(new VersionModel("9.0", "28"));
        versionModels.add(new VersionModel("10.0", "29"));
        return versionModels.get(new Random().nextInt(versionModels.size()));
    }

    public static String gen_random() {
        Random random = new Random();
        StringBuilder stringBuilder = new StringBuilder();
        String key = "1234567890asdfghjklzxcvbnmqwertyuiop";
        for (int i = 0; i < 16; i++) {
            stringBuilder.append(key.charAt(random.nextInt(15)));
        }
        return stringBuilder.toString();
    }

    public static String gen_random_id() {
        Random random = new Random();
        StringBuilder stringBuilder = new StringBuilder();
        String key = "1234567890asdfghjklzxcvbnmqwertyuiop";
        for (int i = 0; i < 6; i++) {
            stringBuilder.append(key.charAt(random.nextInt(5)));
        }
        return stringBuilder.toString().toUpperCase();
    }

    public static String gen_random_ver() {
        Random random = new Random();
        StringBuilder stringBuilder = new StringBuilder();
        String key = "1234567890";
        for (int i = 0; i < 9; i++) {
            stringBuilder.append(key.charAt(random.nextInt(8)));
        }
        return stringBuilder.toString();
    }

    public static String gen_random_brand() {
        String[] brand = {"oneplus", "google", "samsung", "motorola", "xiaomi", "vivo", "realme", "htc", "lg", "sony", "huawei", "oppo", "lenovo", "zte"};
        return brand[new Random().nextInt(brand.length)].toUpperCase();
    }

    public static boolean is_package_installed(String package_name, PackageManager pm) {
        try {
            pm.getPackageInfo(package_name, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String get_launcher_activity_name(PackageManager pm, String s_package) {
        return Objects.requireNonNull(Objects.requireNonNull(pm.getLaunchIntentForPackage(s_package)).getComponent()).getClassName();
    }

    public static void show_loader(SpinKitView spinKitView) {
        spinKitView.setVisibility(View.VISIBLE);
    }

    public static void hide_loader(SpinKitView spinKitView) {
        spinKitView.setVisibility(View.GONE);
    }

    public static String duration(String sec) {
        if (sec == null || Integer.parseInt(sec) < 1500)
            return "1500";
        else
            return sec;
    }
}