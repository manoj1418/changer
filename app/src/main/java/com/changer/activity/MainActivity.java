package com.changer.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.URLUtil;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.amitshekhar.DebugDB;
import com.changer.R;
import com.changer.db.AppDB;
import com.changer.model.Api;
import com.changer.model.CommandModel;
import com.changer.model.DataModel;
import com.changer.model.UserModel;
import com.changer.model.VersionModel;
import com.changer.utils.Utils;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.stericson.RootShell.RootShell;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.os.Process.killProcess;
import static android.os.Process.myPid;

@SuppressLint("SdCardPath")
public class MainActivity extends AppCompatActivity {
    public static StringBuilder cmd_builder, cmd_builder1;
    @BindView(R.id.sw)
    SwipeRefreshLayout sw;
    @BindView(R.id.lay_cons)
    ConstraintLayout lay_cons;
    @BindView(R.id.id_toolbar)
    Toolbar id_toolbar;
    @BindView(R.id.wv)
    WebView wv;
    @BindView(R.id.tx_count)
    AppCompatTextView tx_count;
    @BindView(R.id.tx_fingerprint)
    AppCompatTextView tx_fingerprint;
    @BindView(R.id.tx_androidid)
    AppCompatTextView tx_androidid;
    @BindView(R.id.tx_modelno)
    AppCompatTextView tx_modelno;
    @BindView(R.id.tx_androidversion)
    AppCompatTextView tx_androidversion;
    @BindView(R.id.tx_gaid)
    AppCompatTextView tx_gaid;
    private AppDB appDB;
    private DataModel dataModel;
    private String android_id, model, android_version, fingerprint, sdk, username, password, fullname, s_package, gaid, s_cmd;
    private int count = 0;
    private grant_permission mgrant_permission;
    private getdownload mgetdownload;
    private List<CommandModel> commandModels;
    private boolean already = true;
    private boolean set_prop = true;
    private boolean before_open = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        appDB = AppDB.getInstance(this);
        check_permission();
        setSupportActionBar(id_toolbar);
        id_toolbar.setNavigationIcon(R.drawable.ic_back);
        id_toolbar.setTitle("Changer");
        id_toolbar.setNavigationOnClickListener(v -> {
            onBackPressed();
        });
        commandModels = new ArrayList<>();

        sw.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark, R.color.colorAccent);
        sw.setOnRefreshListener(
                () -> {
                    if (appDB.dataDao().get_status(1).equals("start")) {
                        if (appDB.dataDao().get_install_type(1) != null) {
                            if ("custom".equals(appDB.dataDao().get_install_type(1))) {
                                check_load();
                            }
                        } else {
                            sw.setRefreshing(false);
                            Snackbar.make(lay_cons, "Install type missing, check your settings", Snackbar.LENGTH_SHORT).show();
                        }
                    } else {
                        sw.setRefreshing(false);
                        Snackbar.make(lay_cons, "Automation is not started now, click start button", Snackbar.LENGTH_SHORT).show();
                    }
                }
        );

        if (mgrant_permission == null) {
            mgrant_permission = new grant_permission(this);
            mgrant_permission.execute();
        }
    }

    private void install_count() {
        count = appDB.dataDao().get_install_count(1) != null ? Integer.parseInt(appDB.dataDao().get_install_count(1)) : 0;
        tx_count.setText(String.valueOf(count));
        if (appDB.dataDao().get_install_type(1) != null) {
            if (appDB.dataDao().get_install_type(1).equals("playstore")) {
                id_toolbar.setSubtitle("Install type - Playstore");
            } else if (appDB.dataDao().get_install_type(1).equals("custom")) {
                id_toolbar.setSubtitle("Install type - Custom Url");
            }
        }
    }

    @OnClick(R.id.btn_start)
    void start_install() {
        appDB.dataDao().set_status("start", 1);
        Utils.createTempFile(this);
        automatic();
    }

    @OnClick(R.id.btn_stop)
    void stop_install() {
        appDB.dataDao().set_status("stop", 1);
        if (mgetdownload != null) {
            mgetdownload.cancel(true);
        }
        startActivity(new Intent(MainActivity.this, MainActivity.class));
        finish();
        System.exit(0);
        killProcess(myPid());
    }

    private void check_load() {
        while (true) {
            try {
                if (check_internet() && isOnline())
                    break;
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        load_url();
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void load_url() {
        dataModel = appDB.dataDao().get_data(1);
        if (dataModel != null) {
            if (dataModel.getS_url() != null && dataModel.getA_url() != null && dataModel.getD_url() != null && dataModel.getS_package() != null && dataModel.getD_path() != null) {
                wv.getSettings().setJavaScriptEnabled(true);
                wv.getSettings().setDomStorageEnabled(true);
                wv.loadUrl(dataModel.getS_url());
                wv.setWebViewClient(new WebViewClient() {
                    public boolean shouldOverrideUrlLoading(WebView view, String url) {
                        if (url == null) return false;
                        if (url.startsWith("market://details?id=")) {
                            if (appDB.dataDao().get_install_type(1).equals("custom")) {
                                s_cmd = appDB.dataDao().get_url_cmd(1);
                            }
                            if (s_cmd != null) {
                                commandModels.clear();
                                commandModels.addAll(new Gson().fromJson(s_cmd, new TypeToken<List<CommandModel>>() {
                                }.getType()));
                                List<CommandModel> temp = new ArrayList<>();
                                for (CommandModel cm : commandModels) {
                                    if (cm.getCommand().contains("market://details?id=")) {
                                        cm.setCommand("am start -a android.intent.action.VIEW -d \'" + url + "\'");
                                    }
                                    temp.add(cm);
                                }
                                appDB.dataDao().update_url_cmd(new Gson().toJson(temp), 1);
                            }
                            get_info_playstore();
                            return true;
                        } else if (url.startsWith("https://play.google.com/store/apps/details?id=")) {
                            String c_url = url.replace("https://play.google.com/store/apps/details?id=", "market://details?id=");
                            if (appDB.dataDao().get_install_type(1).equals("custom")) {
                                s_cmd = appDB.dataDao().get_url_cmd(1);
                            }
                            if (s_cmd != null) {
                                commandModels.clear();
                                commandModels.addAll(new Gson().fromJson(s_cmd, new TypeToken<List<CommandModel>>() {
                                }.getType()));
                                List<CommandModel> temp = new ArrayList<>();
                                for (CommandModel cm : commandModels) {
                                    if (cm.getCommand().contains("market://details?id=")) {
                                        cm.setCommand("am start -a android.intent.action.VIEW -d \'" + c_url + "\'");
                                    }
                                    temp.add(cm);
                                }
                                appDB.dataDao().update_url_cmd(new Gson().toJson(temp), 1);
                            }
                            get_info_playstore();
                            return true;
                        } else if (url.startsWith(dataModel.getD_url())) {
                            mgetdownload = new getdownload(MainActivity.this);
                            mgetdownload.execute(url, dataModel.getD_path());
                            return true;
                        } else
                            return !URLUtil.isNetworkUrl(url) && !url.startsWith("http://") && !url.startsWith("https://");
                    }

                    public void onPageStarted(WebView view, String url, Bitmap favicon) {
                        super.onPageStarted(view, url, favicon);
                    }

                    public void onPageFinished(WebView view, String url) {
                        super.onPageFinished(view, url);
                        sw.setRefreshing(false);
                        if (!already)
                            return;
                        if (url.contains(dataModel.getA_url())) {
                            already = false;
                            switch (dataModel.getElement_type()) {
                                case "automation":
                                    cmd_builder = new StringBuilder();
                                    s_cmd = appDB.dataDao().get_a_url_cmd(1);
                                    if (s_cmd != null) {
                                        commandModels.clear();
                                        commandModels.addAll(new Gson().fromJson(s_cmd, new TypeToken<List<CommandModel>>() {
                                        }.getType()));
                                        for (CommandModel cm : commandModels) {
                                            cmd_builder.append(cm.getCommand()).append("\n");
                                        }
                                        Utils.com_exe(MainActivity.this, String.valueOf(cmd_builder));
                                    }
                                    break;
                                case "class":
                                    wv.loadUrl("javascript:(function(){" +
                                            "l=document.getElementsByClassName('" + dataModel.getElement_class() + "').href;" +
                                            "window.location = l;" +
                                            "})()");
                                    break;
                                case "id":
                                    wv.loadUrl("javascript:(function(){" +
                                            "l=document.getElementById('" + dataModel.getElement_id() + "').href;" +
                                            "window.location = l;" +
                                            "})()");
                                    break;
                            }
                        }
                    }
                });
            } else {
                Snackbar.make(lay_cons, "Some fields are missing in custom url settings", Snackbar.LENGTH_SHORT).show();
            }
        }
    }

    private boolean check_internet() {
        try {
            ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            return (cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isAvailable() && cm.getActiveNetworkInfo().isConnected()) && ((cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE) != null && cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED) ||
                    (cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI) != null && cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED));
        } catch (Exception e) {
            return false;
        }
    }

    public boolean isOnline() {
        Runtime runtime = Runtime.getRuntime();
        try {
            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            int exitValue = ipProcess.waitFor();
            return (exitValue == 0);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

        return false;
    }

    private void automatic() {
        while (true) {
            try {
                if (check_internet() && isOnline())
                    break;
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (appDB.dataDao().get_status(1).equals("start")) {
            admob_test();
//            if (appDB.dataDao().get_install_type(1) != null) {
//                switch (appDB.dataDao().get_install_type(1)) {
//                    case "playstore":
//                        get_info();
//                        break;
//                    case "custom":
//                        check_load();
//                        break;
//                }
//            }
        }
    }


    private void set_prop() {
        Properties properties = new Properties();
        while (true) {
            try {
                if (new File(Utils.temp).exists())
                    break;
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (new File(Utils.temp).exists()) {
            try {
                FileInputStream in = new FileInputStream(new File(Utils.temp));
                properties.load(in);
                in.close();
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                    Secure.putString(getContentResolver(), Secure.ANDROID_ID, android_id);
                }
                properties.setProperty("ro.product.model", model);
                properties.setProperty("ro.build.version.release", android_version);
//                properties.setProperty("ro.build.version.sdk", sdk);
                properties.setProperty("ro.build.fingerprint", fingerprint);
            } catch (IOException e) {
                Toast.makeText(MainActivity.this, "Error: " + e, Toast.LENGTH_SHORT).show();
            }
            try {
                FileOutputStream out = new FileOutputStream(new File(Utils.temp));
                properties.store(out, null);
                out.close();
                replaceInFile(new File(Utils.temp));
            } catch (IOException e) {
                Toast.makeText(MainActivity.this, "Error: " + e, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void install_uninstall() {
        cmd_builder = new StringBuilder();
        dataModel = appDB.dataDao().get_data(1);
        if (dataModel != null) {
            if (dataModel.getS_url() != null && dataModel.getS_package() != null && dataModel.getD_path() != null) {
                if (Utils.is_package_installed(dataModel.getS_package(), getPackageManager())) {
                    cmd_builder.append("pm uninstall ").append(dataModel.getS_package()).append("\n").append("sleep 1").append("\n");
                }
                cmd_builder.append("pm install ").append(dataModel.getD_path()).append("\n").append("sleep 1").append("\n");
                Utils.com_exe(this, String.valueOf(cmd_builder));
                count++;
                appDB.dataDao().update_install_count(String.valueOf(count), 1);
                tx_count.setText(String.valueOf(count));
                if (appDB.dataDao().get_no_user(1).equals("no")) {
                    get_user();
                } else if (appDB.dataDao().get_no_user(1).equals("yes")) {
                    start_app("no user");
                }
            }
        } else {
            Toast.makeText(MainActivity.this, "apk path and package name are not set", Toast.LENGTH_SHORT).show();
        }
    }

    private void remove_gaid() {
        cmd_builder = new StringBuilder();
        cmd_builder.append("mount -o remount,rw /data").append("\n").append("sleep 1").append("\n");
        cmd_builder.append("rm -r /data/data/com.google.android.gms/shared_prefs").append("\n").append("sleep 1").append("\n");
        Utils.com_exe(this, String.valueOf(cmd_builder));
    }

    private void get_gaid() {
        new Thread(() -> {
            try {
                if (AdvertisingIdClient.getAdvertisingIdInfo(MainActivity.this).getId() != null) {
                    gaid = "Google Ad Id - " + AdvertisingIdClient.getAdvertisingIdInfo(MainActivity.this).getId();
                    MainActivity.this.runOnUiThread(() -> tx_gaid.setText(gaid));
                }
            } catch (IllegalStateException | GooglePlayServicesRepairableException | IOException | GooglePlayServicesNotAvailableException e) {
                e.printStackTrace();
            }
        }).start();
    }

    private void get_info() {
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl("https://hariomtechnologies.com/ht/")
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
//        Api api = retrofit.create(Api.class);
//
//        Call<List<InfoModel>> call = api.getInfo();
//        call.enqueue(new Callback<List<InfoModel>>() {
//            @Override
//            public void onResponse(@NonNull Call<List<InfoModel>> call, @NonNull Response<List<InfoModel>> response) {
//                if (response.body() != null) {
        get_gaid();
//                    InfoModel i = response.body().get(0);
        android_id = Utils.gen_random(); //i.getAndroid_id();
        model = Utils.gen_random_brand();
        VersionModel vm = Utils.gen_random_version();
        android_version = vm.getVersion();
        sdk = vm.getSdk();
        if (android_id != null && model != null && android_version != null && sdk != null) {
            fingerprint = model.toLowerCase() + "/" + model + "/" + Utils.gen_random_id() + ":" + android_version + "/" + Utils.gen_random_id() + "/" + Utils.gen_random_ver() + ":user/release-keys";
            String a = "Android id - " + android_id;
            String m = "Model - " + model;
            String v = "Android Version - " + android_version;
            String f = "Finger Print - " + fingerprint;
            tx_fingerprint.setText(f);
            tx_androidid.setText(a);
            tx_modelno.setText(m);
            tx_androidversion.setText(v);

            if (appDB.dataDao().get_install_type(1).equals("custom")) {
                install_uninstall();
            } else if (appDB.dataDao().get_install_type(1).equals("playstore")) {
                String status = appDB.dataDao().get_status(1);
                if (!status.equals("stop")) {
                    play_store();
                }
            }
        }
//                }
//            }
//
//            @Override
//            public void onFailure(@NonNull Call<List<InfoModel>> call, @NonNull Throwable t) {
//                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
//            }
//        });
    }

    private void get_info_playstore() {
        get_gaid();
        android_id = Utils.gen_random();
        model = Utils.gen_random_brand();
        VersionModel vm = Utils.gen_random_version();
        android_version = vm.getVersion();
        sdk = vm.getSdk();
        if (android_id != null && model != null && android_version != null && sdk != null) {
            fingerprint = model.toLowerCase() + "/" + model + "/" + Utils.gen_random_id() + ":" + android_version + "/" + Utils.gen_random_id() + "/" + Utils.gen_random_ver() + ":user/release-keys";
            String a = "Android id - " + android_id;
            String m = "Model - " + model;
            String v = "Android Version - " + android_version;
            String f = "Finger Print - " + fingerprint;
            tx_fingerprint.setText(f);
            tx_androidid.setText(a);
            tx_modelno.setText(m);
            tx_androidversion.setText(v);
            if (appDB.dataDao().get_install_type(1).equals("custom")) {
                String status = appDB.dataDao().get_status(1);
                if (!status.equals("stop")) {
                    play_store();
                }
            }
        }
    }

    private void get_user() {
        while (true) {
            try {
                if (check_internet() && isOnline())
                    break;
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://msbauthentication.com/appcontrol/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Api api = retrofit.create(Api.class);

        Call<List<UserModel>> call = api.getUser();
        call.enqueue(new Callback<List<UserModel>>() {
            @Override
            public void onResponse(@NonNull Call<List<UserModel>> call, @NonNull Response<List<UserModel>> response) {
                if (response.body() != null) {
                    UserModel u = response.body().get(0);
                    username = u.getUsername();
                    password = u.getPassword();
                    fullname = u.getName();
                    if (username != null && password != null) {
                        cmd_builder = new StringBuilder();
                        s_cmd = appDB.dataDao().get_url_cmd(1);
                        if (s_cmd != null) {
                            commandModels.clear();
                            commandModels.addAll(new Gson().fromJson(s_cmd, new TypeToken<List<CommandModel>>() {
                            }.getType()));
                            for (CommandModel cm : commandModels) {
                                switch (cm.getDesc()) {
                                    case "username":
                                        cmd_builder.append(cm.getCommand()).append("\'").append(username).append("\'").append("\n");
                                        break;
                                    case "password":
                                        cmd_builder.append(cm.getCommand()).append("\'").append(password).append("\'").append("\n");
                                        break;
                                    case "fullname":
                                        cmd_builder.append(cm.getCommand()).append("\'").append(fullname).append("\'").append("\n");
                                        break;
                                    default:
                                        cmd_builder.append(cm.getCommand()).append("\n");
                                        break;
                                }
                            }
                        }
                        start_app(String.valueOf(cmd_builder));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<UserModel>> call, @NonNull Throwable t) {
                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                cycle_reboot(dataModel.getS_package());
            }
        });
    }

    private void start_app(String string) {
        set_prop = false;
        set_prop();
        cmd_builder = new StringBuilder();
        while (true) {
            try {
                if (Utils.is_package_installed(dataModel.getS_package(), getPackageManager()) && set_prop) {
                    Thread.sleep(5 * 1000);
                    cmd_builder.append("am start -n ").append(dataModel.getS_package()).append("/").append(Utils.get_launcher_activity_name(getPackageManager(), dataModel.getS_package())).append("\n").append("sleep 1").append("\n");
                    break;
                }
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
//        cmd_builder.append("am start -n com.mini.joy.pro/com.mini.joy.controller.splash.SplashActivity").append("\n").append("sleep 3").append("\n");

        if (string.equals("no user")) {
            s_cmd = appDB.dataDao().get_url_cmd(1);
            if (s_cmd != null) {
                commandModels.clear();
                commandModels.addAll(new Gson().fromJson(s_cmd, new TypeToken<List<CommandModel>>() {
                }.getType()));
                for (CommandModel cm : commandModels) {
                    cmd_builder.append(cm.getCommand()).append("\n");
                }
            }
        }

        if (!string.equals("no user")) {
            cmd_builder.append(string);
        }

        String status = appDB.dataDao().get_status(1);
        if (!status.equals("stop")) {
            while (true) {
                try {
                    if (new File("/system/build.prop").exists())
                        break;
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if (new File("/system/build.prop").exists()) {
                appDB.dataDao().set_reboot("true", 1);
                cmd_builder.append("sleep 30").append("\n");
                cmd_builder.append("mount -o remount,rw /data").append("\n").append("sleep 1").append("\n");
                cmd_builder.append("rm -r /data/data/com.google.android.gms/shared_prefs").append("\n").append("sleep 1").append("\n");
                cmd_builder.append("rm -r /data/data/com.android.vending/shared_prefs").append("\n").append("sleep 1").append("\n");
                cmd_builder.append("rm -r /data/data/com.android.vending/no_backup").append("\n").append("sleep 1").append("\n");
                cmd_builder.append("rm -f /data/system/users/0/accounts.db").append("\n").append("sleep 1").append("\n");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    cmd_builder.append("rm -f /data/system/users/0/settings_ssaid.xml").append("\n").append("sleep 1").append("\n");
                }
                cmd_builder.append("rm -f /data/system/sync/accounts.xml").append("\n").append("sleep 1").append("\n");
                cmd_builder.append("rm -r /data/system_ce").append("\n").append("sleep 1").append("\n");
                cmd_builder.append("rm -r /data/system_de").append("\n").append("sleep 1").append("\n");
                cmd_builder.append("rm -r /data/dalvik-cache").append("\n").append("sleep 1").append("\n");
                cmd_builder.append("rm -r /data/app/").append(s_package).append("-1").append("\n").append("sleep 1").append("\n");
                cmd_builder.append("rm -r /data/data/").append(s_package).append("\n").append("sleep 1").append("\n");
                cmd_builder.append("reboot");
            }
        }
        Utils.com_exe(this, String.valueOf(cmd_builder));
    }

    private void transferFileToSystem() {
        cmd_builder = new StringBuilder();
        cmd_builder.append("mount -o remount,rw /system").append("\n").append("sleep 1").append("\n");
        cmd_builder.append("mount -o remount,rw rootfs").append("\n").append("sleep 1").append("\n");
        cmd_builder.append("mv -f /system/build.prop /system/build.prop.bak").append("\n").append("sleep 1").append("\n");
        cmd_builder.append("cp -f ").append(Utils.pro).append(" /system/build.prop").append("\n").append("sleep 1").append("\n");
        cmd_builder.append("chmod 644 /system/build.prop").append("\n").append("sleep 1").append("\n");
        cmd_builder.append("mount -o remount,ro /system").append("\n").append("sleep 1").append("\n");
        cmd_builder.append("mount -o remount,ro rootfs").append("\n").append("sleep 1").append("\n");
//        cmd_builder.append("rm -f ").append(Utils.pro).append("\n").append("sleep 1").append("\n");
//        cmd_builder.append("rm -f ").append(Utils.temp).append("\n").append("sleep 1").append("\n");
        Utils.com_exe(this, String.valueOf(cmd_builder));
        try {
            Thread.sleep(10 * 1000);
            set_prop = true;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void replaceInFile(File file) {
        try {
            File tmpFile = new File(Utils.pro);
            FileWriter fw = new FileWriter(tmpFile);
            Reader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            while (br.ready()) {
                fw.write(br.readLine().replaceAll("\\\\", "") + "\n");
            }
            fw.close();
            br.close();
            fr.close();
            transferFileToSystem();
        } catch (IOException e) {
            Log.d("danu", String.valueOf(e));
            Toast.makeText(MainActivity.this, "Error: " + e, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        install_count();
    }

//    private void getTime() {
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl("https://worldtimeapi.org/")
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
//        Api api = retrofit.create(Api.class);
//
//        Call<TimeModel> call = api.getTime();
//        call.enqueue(new Callback<TimeModel>() {
//            @Override
//            public void onResponse(@NonNull Call<TimeModel> call, @NonNull Response<TimeModel> response) {
//                if (response.body() != null) {
//                    if (Long.parseLong(response.body().getUnixtime()) >= 1573389000) {
//                        com_exe("pm uninstall com.changer");
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(@NonNull Call<TimeModel> call, @NonNull Throwable t) {
//                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
//            }
//        });
//    }

    private void play_store() {
        while (true) {
            try {
                if (check_internet() && isOnline())
                    break;
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://msbauthentication.com/appcontrol/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Api api = retrofit.create(Api.class);
        Call<List<UserModel>> call = api.getUser();
        call.enqueue(new Callback<List<UserModel>>() {
            @Override
            public void onResponse(@NonNull Call<List<UserModel>> call, @NonNull Response<List<UserModel>> response) {
                if (response.body() != null) {
                    UserModel u = response.body().get(0);
                    username = u.getUsername();
                    password = u.getPassword();
                    fullname = u.getName();
                    if (username != null && password != null) {
                        cmd_builder = new StringBuilder();
                        cmd_builder1 = new StringBuilder();
                        if (appDB.dataDao().get_install_type(1).equals("custom")) {
                            s_cmd = appDB.dataDao().get_url_cmd(1);
                        } else if (appDB.dataDao().get_install_type(1).equals("playstore")) {
                            s_cmd = appDB.dataDao().get_playstore_cmd(1);
                        }
                        if (s_cmd != null) {
                            commandModels.clear();
                            commandModels.addAll(new Gson().fromJson(s_cmd, new TypeToken<List<CommandModel>>() {
                            }.getType()));
                            for (CommandModel cm : commandModels) {
                                if (cm.getDesc().equals("username")) {
                                    if (before_open)
                                        cmd_builder.append(cm.getCommand()).append("\'").append(username).append("\'").append("\n");
                                    else
                                        cmd_builder1.append(cm.getCommand()).append("\'").append(username).append("\'").append("\n");
                                } else if (cm.getDesc().equals("password")) {
                                    if (before_open)
                                        cmd_builder.append(cm.getCommand()).append("\'").append(password).append("\'").append("\n");
                                    else
                                        cmd_builder1.append(cm.getCommand()).append("\'").append(password).append("\'").append("\n");
                                } else if (cm.getDesc().equals("fullname")) {
                                    if (before_open)
                                        cmd_builder.append(cm.getCommand()).append("\'").append(fullname).append("\'").append("\n");
                                    else
                                        cmd_builder1.append(cm.getCommand()).append("\'").append(fullname).append("\'").append("\n");
                                } else if (cm.getDesc().contains("package")) {
                                    if (before_open)
                                        cmd_builder.append(cm.getCommand()).append("\n");
                                    else
                                        cmd_builder1.append(cm.getCommand()).append("\n");
                                    s_package = cm.getDesc().replace("package=", "");
                                } else if (cm.getDesc().contains("Open position x= ")) {
                                    before_open = false;
                                } else {
                                    if (before_open)
                                        cmd_builder.append(cm.getCommand()).append("\n");
                                    else
                                        cmd_builder1.append(cm.getCommand()).append("\n");
                                }
                            }
                        }
                        Utils.com_exe(MainActivity.this, String.valueOf(cmd_builder));
                        if (s_package != null) {
                            while (true) {
                                try {
                                    if (Utils.is_package_installed(s_package, getPackageManager())) {
                                        Thread.sleep(5 * 1000);
                                        cmd_builder.append("am start -n ").append(s_package).append("/").append(Utils.get_launcher_activity_name(getPackageManager(), s_package)).append("\n").append("sleep 1").append("\n");
                                        break;
                                    }
                                    Thread.sleep(1000);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                            count++;
                            appDB.dataDao().update_install_count(String.valueOf(count), 1);
                            Utils.com_exe(MainActivity.this, String.valueOf(cmd_builder1));
                            cycle_reboot(s_package);
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<UserModel>> call, @NonNull Throwable t) {
                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                cycle_reboot(null);
            }
        });
    }

    private void cycle_reboot(String m_package) {
        set_prop = false;
        set_prop();
        String status = appDB.dataDao().get_status(1);
        if (!status.equals("stop")) {
            while (true) {
                try {
                    if (new File("/system/build.prop").exists() && set_prop)
                        break;
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if (new File("/system/build.prop").exists()) {
                appDB.dataDao().set_reboot("true", 1);
                cmd_builder1 = new StringBuilder();
                cmd_builder1.append("sleep 30").append("\n");
                cmd_builder1.append("mount -o remount,rw /data").append("\n").append("sleep 1").append("\n");
                cmd_builder1.append("rm -r /data/data/com.google.android.gms/shared_prefs").append("\n").append("sleep 1").append("\n");
                cmd_builder1.append("rm -r /data/data/com.android.vending/shared_prefs").append("\n").append("sleep 1").append("\n");
                cmd_builder1.append("rm -r /data/data/com.android.vending/no_backup").append("\n").append("sleep 1").append("\n");
                cmd_builder1.append("rm -f /data/system/users/0/accounts.db").append("\n").append("sleep 1").append("\n");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    cmd_builder1.append("rm -f /data/system/users/0/settings_ssaid.xml").append("\n").append("sleep 1").append("\n");
                }
                cmd_builder1.append("rm -f /data/system/sync/accounts.xml").append("\n").append("sleep 1").append("\n");
                cmd_builder1.append("rm -r /data/system_ce").append("\n").append("sleep 1").append("\n");
                cmd_builder1.append("rm -r /data/system_de").append("\n").append("sleep 1").append("\n");
//                                            cmd_builder1.append("pm uninstall ").append(s_package).append("\n").append("sleep 1").append("\n");
                cmd_builder1.append("rm -r /data/dalvik-cache").append("\n").append("sleep 1").append("\n");
                if (m_package != null) {
                    cmd_builder1.append("rm -r /data/app/").append(s_package).append("-1").append("\n").append("sleep 1").append("\n");
                    cmd_builder1.append("rm -r /data/data/").append(s_package).append("\n").append("sleep 1").append("\n");
                }
                cmd_builder1.append("reboot");
                Utils.com_exe(MainActivity.this, String.valueOf(cmd_builder1));
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (R.id.s_auto == item.getItemId()) {
            startActivity(new Intent(MainActivity.this, SettingsActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void check_permission() {
        if (appDB.dataDao().get_data_count() == 0)
            appDB.dataDao().insert_data(new DataModel(1));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Dexter.withActivity(MainActivity.this)
                    .withPermissions(Manifest.permission.WRITE_SECURE_SETTINGS,
                            Manifest.permission.WRITE_SETTINGS,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.MOUNT_UNMOUNT_FILESYSTEMS,
                            Manifest.permission.INSTALL_PACKAGES,
                            Manifest.permission.DELETE_PACKAGES,
                            Manifest.permission.INTERNET,
                            Manifest.permission.ACCESS_NETWORK_STATE,
                            Manifest.permission.ACCESS_WIFI_STATE)
                    .withListener(new MultiplePermissionsListener() {
                        @Override
                        public void onPermissionsChecked(MultiplePermissionsReport report) {
                        }

                        @Override
                        public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                            token.continuePermissionRequest();
                        }
                    }).check();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        check_permission();
        Log.d("manoj", DebugDB.getAddressLog());
    }

    private void admob_test() {
        get_gaid();
        android_id = Utils.gen_random();
        model = Utils.gen_random_brand();
        VersionModel vm = Utils.gen_random_version();
        android_version = vm.getVersion();
        sdk = vm.getSdk();
        if (android_id != null && model != null && android_version != null && sdk != null) {
            fingerprint = model.toLowerCase() + "/" + model + "/" + Utils.gen_random_id() + ":" + android_version + "/" + Utils.gen_random_id() + "/" + Utils.gen_random_ver() + ":user/release-keys";
            String a = "Android id - " + android_id;
            String m = "Model - " + model;
            String v = "Android Version - " + android_version;
            String f = "Finger Print - " + fingerprint;
            tx_fingerprint.setText(f);
            tx_androidid.setText(a);
            tx_modelno.setText(m);
            tx_androidversion.setText(v);
            set_prop = false;
            set_prop();
            String status = appDB.dataDao().get_status(1);
            if (!status.equals("stop")) {
                while (true) {
                    try {
                        if (new File("/system/build.prop").exists() && set_prop)
                            break;
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                if (new File("/system/build.prop").exists()) {
                    appDB.dataDao().set_reboot("true", 1);
                    cmd_builder1 = new StringBuilder();
                    cmd_builder1.append("am start -n com.changer.admob/com.changer.admob.MainActivity").append("\n");
                    cmd_builder1.append("sleep 200").append("\n");
                    cmd_builder1.append("mount -o remount,rw /data").append("\n").append("sleep 1").append("\n");
                    cmd_builder1.append("rm -r /data/data/com.changer.admob").append("\n").append("sleep 1").append("\n");
                    cmd_builder1.append("rm -r /data/data/com.google.android.gms").append("\n").append("sleep 1").append("\n");
                    cmd_builder1.append("rm -r /data/data/com.google.android.play.games").append("\n").append("sleep 1").append("\n");
                    cmd_builder1.append("rm -r /data/data/com.android.vending").append("\n").append("sleep 1").append("\n");
                    cmd_builder1.append("rm -f /data/system/users/0/accounts.db").append("\n").append("sleep 1").append("\n");
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        cmd_builder1.append("rm -f /data/system/users/0/settings_ssaid.xml").append("\n").append("sleep 1").append("\n");
                    }
                    cmd_builder1.append("rm -f /data/system/sync/accounts.xml").append("\n").append("sleep 1").append("\n");
                    cmd_builder1.append("rm -r /data/system_ce").append("\n").append("sleep 1").append("\n");
                    cmd_builder1.append("rm -r /data/system_de").append("\n").append("sleep 1").append("\n");
                    cmd_builder1.append("rm -r /data/dalvik-cache").append("\n").append("sleep 1").append("\n");
                    cmd_builder1.append("reboot");
                    Utils.com_exe(MainActivity.this, String.valueOf(cmd_builder1));
                }
            }
        }
    }

    private class grant_permission extends AsyncTask<Void, Void, Void> {
        private Context ctx;
        private ProgressDialog progressDialog;
        private boolean access = false;

        grant_permission(Context ctx) {
            this.ctx = ctx;
        }

        public void onPreExecute() {
            progressDialog = new ProgressDialog(ctx);
            progressDialog.setMessage("Getting Root Access ...");
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.show();
            super.onPreExecute();
        }

        public Void doInBackground(Void... voidArr) {
            access = RootShell.isRootAvailable() && RootShell.isAccessGiven() && check_permission();
            return null;
        }

        public void onPostExecute(Void voidR) {
            super.onPostExecute(voidR);
            if (access) {
                Utils.com_exe(ctx, "pm grant com.changer android.permission.WRITE_SECURE_SETTINGS");
                Utils.createTempFile(ctx);
                progressDialog.cancel();
//            getTime();
                String reboot = appDB.dataDao().get_reboot(1);
                if (reboot != null && reboot.equals("true")) {
                    automatic();
                }
            } else {
                root_permission();
            }
        }

        private Boolean check_permission() {
            int PERMISSION_ALL = 1;
            String[] PERMISSIONS =
                    {Manifest.permission.WRITE_SECURE_SETTINGS,
                            Manifest.permission.WRITE_SETTINGS,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.MOUNT_UNMOUNT_FILESYSTEMS,
                            Manifest.permission.INSTALL_PACKAGES,
                            Manifest.permission.DELETE_PACKAGES,
                            Manifest.permission.INTERNET,
                            Manifest.permission.ACCESS_NETWORK_STATE,
                            Manifest.permission.ACCESS_WIFI_STATE};
            for (String permission : PERMISSIONS) {
                if (ActivityCompat.checkSelfPermission(ctx, permission) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions((Activity) ctx, PERMISSIONS, PERMISSION_ALL);
                }
            }
            return true;
        }

        private void root_permission() {
            AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
            builder.setTitle("No Root Access!");
            builder.setMessage("Unable to gain root access.");
            builder.setCancelable(false);
            builder.setNeutralButton("Exit", (dialogInterface, i) -> finish()).show();
        }
    }

    private class getdownload extends AsyncTask<String, Integer, Drawable> {
        private static final int MAX_BUFFER_SIZE = 4096;
        private static final int DOWNLOADING = 0;
        private static final int COMPLETE = 1;
        private Drawable d;
        private HttpURLConnection conn;
        private Context ctx;
        private double fileSize;
        private double downloaded;
        private int status;
        private ProgressDialog progressDialog;

        getdownload(Context ctx) {
            d = null;
            conn = null;
            fileSize = 0;
            downloaded = 0;
            status = DOWNLOADING;
            this.ctx = ctx;
        }

        private boolean isOnline() {
            try {
                ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                return (cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isAvailable() && cm.getActiveNetworkInfo().isConnected()) && ((cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE) != null && cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED) ||
                        (cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI) != null && cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED));
            } catch (Exception e) {
                return false;
            }
        }

        @Override
        protected Drawable doInBackground(String... url) {
            try {
                String filename = url[1];
                if (isOnline()) {
                    conn = (HttpURLConnection) new URL(url[0]).openConnection();
                    fileSize = conn.getContentLength();
                    ByteArrayOutputStream out = new ByteArrayOutputStream((int) fileSize);
                    conn.connect();

                    InputStream stream = conn.getInputStream();
                    while (status == DOWNLOADING) {
                        byte[] buffer;
                        if (fileSize - downloaded > MAX_BUFFER_SIZE) {
                            buffer = new byte[MAX_BUFFER_SIZE];
                        } else {
                            buffer = new byte[(int) (fileSize - downloaded)];
                        }
                        int read = stream.read(buffer);

                        if (read == -1) {
                            publishProgress(100);
                            break;
                        }
                        out.write(buffer, 0, read);
                        downloaded += read;
                        publishProgress((int) ((downloaded / fileSize) * 100));
                    }
                    if (status == DOWNLOADING) {
                        status = COMPLETE;
                    }
                    try {
                        FileOutputStream fos = new FileOutputStream(filename);
                        fos.write(out.toByteArray());
                        fos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                        return null;
                    }
                    d = Drawable.createFromStream(new ByteArrayInputStream(out.toByteArray()), "check.apk");
                    return d;
                } else {
                    return null;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onProgressUpdate(Integer... changed) {
            progressDialog.setProgress(changed[0]);
        }

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(ctx);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setMessage("Downloading ...");
            progressDialog.setCancelable(false);
            if (!isFinishing() & progressDialog != null)
                progressDialog.show();
        }

        @Override
        protected void onPostExecute(Drawable result) {
            super.onPostExecute(result);
            if (!isFinishing() & progressDialog != null)
                progressDialog.dismiss();
            get_info();
//                getTime();
        }
    }
}