package com.changer.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.changer.R;
import com.changer.model.Api;
import com.changer.model.DeviceModel;
import com.changer.utils.Item_Space_Decoration;
import com.changer.utils.M_Listener;
import com.changer.utils.Utils;
import com.github.ybq.android.spinkit.SpinKitView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DeviceActivity extends AppCompatActivity implements M_Listener {
    @BindView(R.id.lay_cons)
    ConstraintLayout lay_cons;
    @BindView(R.id.id_toolbar)
    Toolbar id_toolbar;
    @BindView(R.id.id_rv)
    RecyclerView id_rv;
    @BindView(R.id.id_progress)
    SpinKitView id_progress;
    private List<DeviceModel> deviceModels;
    private DeviceAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device);
        ButterKnife.bind(this);

        setSupportActionBar(id_toolbar);
        id_toolbar.setNavigationIcon(R.drawable.ic_back);
        id_toolbar.setTitle("Choose Device");
        id_toolbar.setNavigationOnClickListener(v -> onBackPressed());

        deviceModels = new ArrayList<>();

        id_rv.setHasFixedSize(true);
        id_rv.setLayoutManager(new LinearLayoutManager(this));
        id_rv.addItemDecoration(new Item_Space_Decoration(16, 1));
        id_rv.setItemAnimator(new DefaultItemAnimator());
        adapter = new DeviceAdapter(deviceModels, this);
        id_rv.setAdapter(adapter);

        get_data();
    }

    private void get_data() {
        Utils.show_loader(id_progress);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://msbauthentication.com/super/appcontrol/webapi/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Api api = retrofit.create(Api.class);

        Call<List<DeviceModel>> call = api.getDevice();
        call.enqueue(new Callback<List<DeviceModel>>() {
            @Override
            public void onResponse(@NonNull Call<List<DeviceModel>> call, @NonNull Response<List<DeviceModel>> response) {
                Utils.hide_loader(id_progress);
                if (response.body() != null) {
                    deviceModels.clear();
                    deviceModels.addAll(response.body());
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<DeviceModel>> call, @NonNull Throwable t) {
                Utils.hide_loader(id_progress);
                Toast.makeText(DeviceActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (R.id.s_refresh == item.getItemId()) {
            get_data();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void Click(int position) {
        Intent intent = new Intent(DeviceActivity.this, CampaignActivity.class);
        intent.putExtra("device_id", deviceModels.get(position).getId());
        startActivity(intent);
    }
}