package com.changer.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.changer.R;
import com.changer.db.AppDB;
import com.changer.model.DataModel;
import com.changer.utils.Utils;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CustomUrlActivity extends AppCompatActivity {
    @BindView(R.id.lay_cons)
    ConstraintLayout lay_cons;
    @BindView(R.id.id_toolbar)
    Toolbar id_toolbar;
    @BindView(R.id.ed_s_url)
    AppCompatEditText ed_s_url;
    @BindView(R.id.ed_a_url)
    AppCompatEditText ed_a_url;
    @BindView(R.id.ed_d_url)
    AppCompatEditText ed_d_url;
    @BindView(R.id.ed_s_package)
    AppCompatEditText ed_s_package;
    @BindView(R.id.ed_d_path)
    AppCompatEditText ed_d_path;
    @BindView(R.id.ch_direct)
    AppCompatCheckBox ch_direct;
    @BindView(R.id.ch_automation)
    AppCompatCheckBox ch_automation;
    @BindView(R.id.ch_class)
    AppCompatCheckBox ch_class;
    @BindView(R.id.ch_id)
    AppCompatCheckBox ch_id;
    @BindView(R.id.tx_automation)
    AppCompatTextView tx_automation;
    @BindView(R.id.ed_ele_class)
    AppCompatEditText ed_ele_class;
    @BindView(R.id.ed_ele_id)
    AppCompatEditText ed_ele_id;
    private AppDB appDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_url);
        ButterKnife.bind(this);
        appDB = AppDB.getInstance(this);

        id_toolbar.setNavigationIcon(R.drawable.ic_back);
        id_toolbar.setTitle("Custom Url Settings");
        id_toolbar.setNavigationOnClickListener(v -> {
            onBackPressed();
        });

        clear_all();

        if (appDB.dataDao().get_download_path(1) == null)
            appDB.dataDao().update_download_path(Utils.m_sdcard + "/Download/check.apk", 1);

        DataModel dataModel = appDB.dataDao().get_data(1);
        if (dataModel != null) {
            ed_s_url.setText(dataModel.getS_url());
            ed_a_url.setText(dataModel.getA_url());
            ed_d_url.setText(dataModel.getD_url());
            ed_s_package.setText(dataModel.getS_package());
            ed_d_path.setText(dataModel.getD_path());
            ed_ele_class.setText(dataModel.getElement_class());
            ed_ele_id.setText(dataModel.getElement_id());
        }

        if (appDB.dataDao().get_element_type(1) != null) {
            switch (appDB.dataDao().get_element_type(1)) {
                case "direct":
                    ch_direct.setChecked(true);
                    id_toolbar.setSubtitle("Download type - Direct");
                    break;
                case "automation":
                    ch_automation.setChecked(true);
                    tx_automation.setVisibility(View.VISIBLE);
                    id_toolbar.setSubtitle("Download type - Automation");
                    break;
                case "class":
                    ch_class.setChecked(true);
                    ed_ele_class.setVisibility(View.VISIBLE);
                    id_toolbar.setSubtitle("Download type - With Class");
                    break;
                case "id":
                    ch_id.setChecked(true);
                    ed_ele_id.setVisibility(View.VISIBLE);
                    id_toolbar.setSubtitle("Download type - With ID");
                    break;
            }
        }

        ch_direct.setOnCheckedChangeListener((buttonView, isChecked) -> {
            clear_all();
            ch_direct.setChecked(isChecked);
            if (isChecked) {
                appDB.dataDao().update_element_type("direct", 1);
                id_toolbar.setSubtitle("Download type - Direct");
            }
        });

        ch_automation.setOnCheckedChangeListener((buttonView, isChecked) -> {
            clear_all();
            ch_automation.setChecked(isChecked);
            tx_automation.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            if (isChecked) {
                appDB.dataDao().update_element_type("automation", 1);
                id_toolbar.setSubtitle("Download type - Automation");
            }
        });

        ch_class.setOnCheckedChangeListener((buttonView, isChecked) -> {
            clear_all();
            ch_class.setChecked(isChecked);
            ed_ele_class.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            if (isChecked) {
                appDB.dataDao().update_element_type("class", 1);
                appDB.dataDao().update_element_class(String.valueOf(ed_ele_class.getText()), 1);
                id_toolbar.setSubtitle("Download type - With Class");
            }
        });

        ch_id.setOnCheckedChangeListener((buttonView, isChecked) -> {
            clear_all();
            ch_id.setChecked(isChecked);
            ed_ele_id.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            if (isChecked) {
                appDB.dataDao().update_element_type("id", 1);
                appDB.dataDao().update_element_id(String.valueOf(ed_ele_id.getText()), 1);
                id_toolbar.setSubtitle("Download type - With ID");
            }
        });
    }

    @OnClick(R.id.tx_automation)
    void automation() {
        Intent intent = new Intent(CustomUrlActivity.this, AutoActivity.class);
        intent.putExtra("data", "action_cmd");
        startActivity(intent);
    }

    @OnClick(R.id.btn_submit)
    void submit() {
        if (!new File(Utils.backup).exists())
            Utils.backup(this, lay_cons);
        if (Utils.isEmpty(ed_s_url)) {
            Snackbar.make(lay_cons, "please fill the source url", Snackbar.LENGTH_SHORT).show();
            return;
        }
        if (Utils.isEmpty(ed_a_url)) {
            Snackbar.make(lay_cons, "please fill the action url", Snackbar.LENGTH_SHORT).show();
            return;
        }
        if (Utils.isEmpty(ed_d_url)) {
            Snackbar.make(lay_cons, "please fill the download url", Snackbar.LENGTH_SHORT).show();
            return;
        }
        if (Utils.isEmpty(ed_s_package)) {
            Snackbar.make(lay_cons, "please fill the package name", Snackbar.LENGTH_SHORT).show();
            return;
        }
        if (Utils.isEmpty(ed_d_path)) {
            Snackbar.make(lay_cons, "please fill the download path", Snackbar.LENGTH_SHORT).show();
            return;
        }
        if (ch_direct.isChecked() || ch_automation.isChecked() || ch_class.isChecked() || ch_id.isChecked()) {
            if (ch_class.isChecked()) {
                if (Utils.isEmpty(ed_ele_class)) {
                    Snackbar.make(lay_cons, "please fill the class name", Snackbar.LENGTH_SHORT).show();
                    return;
                }
                appDB.dataDao().update_element_class(String.valueOf(ed_ele_class.getText()), 1);
            }
            if (ch_id.isChecked()) {
                if (Utils.isEmpty(ed_ele_id)) {
                    Snackbar.make(lay_cons, "please fill the element id", Snackbar.LENGTH_SHORT).show();
                    return;
                }
                appDB.dataDao().update_element_id(String.valueOf(ed_ele_id.getText()), 1);
            }
            if (update_db() == 1) {
                Snackbar.make(lay_cons, "Successfully Stored", Snackbar.LENGTH_SHORT).show();
            } else {
                Snackbar.make(lay_cons, "Failed, Retry", Snackbar.LENGTH_SHORT).show();
            }
        } else {
            Snackbar.make(lay_cons, "please select any one method of download", Snackbar.LENGTH_SHORT).show();
        }
    }

    private int update_db() {
        return appDB.dataDao().update_url_type(String.valueOf(ed_s_url.getText()), String.valueOf(ed_a_url.getText()), String.valueOf(ed_d_url.getText()), String.valueOf(ed_s_package.getText()), String.valueOf(ed_d_path.getText()), 1);
    }

    private void clear_all() {
        ch_direct.setChecked(false);
        ch_automation.setChecked(false);
        ch_class.setChecked(false);
        ch_id.setChecked(false);
        tx_automation.setVisibility(View.GONE);
        ed_ele_class.setVisibility(View.GONE);
        ed_ele_id.setVisibility(View.GONE);
        id_toolbar.setSubtitle("");
    }
}