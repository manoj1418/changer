package com.changer.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.changer.R;
import com.changer.db.AppDB;
import com.changer.model.Api;
import com.changer.model.CampaignModel;
import com.changer.model.CommandModel;
import com.changer.utils.Item_Space_Decoration;
import com.changer.utils.M_Listener;
import com.changer.utils.Utils;
import com.github.ybq.android.spinkit.SpinKitView;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CampaignActivity extends AppCompatActivity implements M_Listener {
    @BindView(R.id.lay_cons)
    ConstraintLayout lay_cons;
    @BindView(R.id.id_toolbar)
    Toolbar id_toolbar;
    @BindView(R.id.id_rv)
    RecyclerView id_rv;
    @BindView(R.id.id_progress)
    SpinKitView id_progress;
    private CampaignModel campaign;
    private List<CampaignModel> campaignModels;
    private List<CommandModel> commandModel;
    private CampaignAdapter adapter;
    private AppDB appDB;
    private String device_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_campaign);
        ButterKnife.bind(this);
        appDB = AppDB.getInstance(this);

        setSupportActionBar(id_toolbar);
        id_toolbar.setNavigationIcon(R.drawable.ic_back);
        id_toolbar.setTitle("Choose Device");
        id_toolbar.setNavigationOnClickListener(v -> onBackPressed());

        commandModel = new ArrayList<>();
        campaignModels = new ArrayList<>();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null)
            device_id = bundle.getString("device_id");

        id_rv.setHasFixedSize(true);
        id_rv.setLayoutManager(new LinearLayoutManager(this));
        id_rv.addItemDecoration(new Item_Space_Decoration(16, 1));
        id_rv.setItemAnimator(new DefaultItemAnimator());
        adapter = new CampaignAdapter(campaignModels, this);
        id_rv.setAdapter(adapter);

        get_data();
    }

    private void get_data() {
        if (device_id != null) {
            Utils.show_loader(id_progress);
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://msbauthentication.com/super/appcontrol/webapi/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            Api api = retrofit.create(Api.class);

            Call<List<CampaignModel>> call = api.getCampaign(device_id);
            call.enqueue(new Callback<List<CampaignModel>>() {
                @Override
                public void onResponse(@NonNull Call<List<CampaignModel>> call, @NonNull Response<List<CampaignModel>> response) {
                    Utils.hide_loader(id_progress);
                    if (response.body() != null) {
                        campaignModels.clear();
                        campaignModels.addAll(response.body());
                        adapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<List<CampaignModel>> call, @NonNull Throwable t) {
                    Utils.hide_loader(id_progress);
                    Toast.makeText(CampaignActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (R.id.s_refresh == item.getItemId()) {
            get_data();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void Click(int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to activate new campaign settings ?")
                .setCancelable(false)
                .setPositiveButton("Yes", (dialog, id) -> {
                    campaign = campaignModels.get(position);
                    if (campaign != null)
                        get_tasks(campaign.getCamp_id());
                })
                .setNegativeButton("No", (dialog, id) -> dialog.dismiss());
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void get_tasks(String campaignid) {
        if (campaignid != null) {
            Utils.show_loader(id_progress);
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://msbauthentication.com/super/appcontrol/webapi/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            Api api = retrofit.create(Api.class);

            Call<CampaignModel> call = api.get_all_tasks(campaignid);
            call.enqueue(new Callback<CampaignModel>() {
                @Override
                public void onResponse(@NonNull Call<CampaignModel> call, @NonNull Response<CampaignModel> response) {
                    Utils.hide_loader(id_progress);
                    if (response.body() != null) {
                        campaign = response.body();
                        appDB.dataDao().update_task(campaign.getAutomation().equals("1") ? "on" : "off",
                                campaign.getInstall_type(),
                                campaign.getS_url(),
                                campaign.getA_url(),
                                campaign.getD_url(),
                                campaign.getS_package(),
                                campaign.getD_path() == null ? Utils.m_sdcard + "/Download/check.apk" : campaign.getD_path(),
                                campaign.getElement_type(),
                                campaign.getElement_class(),
                                campaign.getElement_id(),
                                add_cmd(campaign.getPlaystore_cmd()),
                                add_cmd(campaign.getUrl_cmd()),
                                add_cmd(campaign.getA_url_cmd()),
                                1);
                        startActivity(new Intent(CampaignActivity.this, SettingsActivity.class));
                        finish();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CampaignModel> call, @NonNull Throwable t) {
                    Utils.hide_loader(id_progress);
                    Toast.makeText(CampaignActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    private String add_cmd(List<CommandModel> cmd) {
        if (cmd != null && cmd.size() > 0) {
            for (CommandModel cm : cmd) {
                if (cm == null) return "";
                String x, y, x1, y1, input, sec;
                if (cm.getType().equals("click")) {
                    x = cm.getX().trim();
                    y = cm.getY().trim();
                    if (x.equals("")) {
                        Snackbar.make(lay_cons, "x position value is empty", Snackbar.LENGTH_SHORT).show();
                        return "";
                    }
                    if (y.equals("")) {
                        Snackbar.make(lay_cons, "y position value is empty", Snackbar.LENGTH_SHORT).show();
                        return "";
                    }
                    commandModel.add(add_list("click", "input tap " + x + " " + y, "click position x= " + x + " y= " + y, x, y, "", "", "", ""));
                }
                if (cm.getType().equals("swipe")) {
                    x = cm.getX().trim();
                    y = cm.getY().trim();
                    x1 = cm.getX1().trim();
                    y1 = cm.getY1().trim();
                    sec = Utils.duration(cm.getSleep().trim());
                    if (x.equals("")) {
                        Snackbar.make(lay_cons, "x position value is empty", Snackbar.LENGTH_SHORT).show();
                        return "";
                    }
                    if (y.equals("")) {
                        Snackbar.make(lay_cons, "y position value is empty", Snackbar.LENGTH_SHORT).show();
                        return "";
                    }
                    if (x1.equals("")) {
                        Snackbar.make(lay_cons, "x1 position value is empty", Snackbar.LENGTH_SHORT).show();
                        return "";
                    }
                    if (y1.equals("")) {
                        Snackbar.make(lay_cons, "y1 position value is empty", Snackbar.LENGTH_SHORT).show();
                        return "";
                    }
                    if (sec.equals("")) {
                        Snackbar.make(lay_cons, "duration second is empty", Snackbar.LENGTH_SHORT).show();
                        return "";
                    }
                    commandModel.add(add_list("swipe", "input swipe " + x + " " + y + " " + x1 + " " + y1 + " " + sec, "swipe position x= " + x + " y= " + y + " x1= " + x1 + " y1= " + y1 + " duration= " + sec, x, y, x1, y1, "", sec));
                }
                if (cm.getType().equals("text")) {
                    x = cm.getX().trim();
                    y = cm.getX().trim();
                    input = cm.getText().toLowerCase().trim();
                    if (x.equals("")) {
                        Snackbar.make(lay_cons, "x position value is empty", Snackbar.LENGTH_SHORT).show();
                        return "";
                    }
                    if (y.equals("")) {
                        Snackbar.make(lay_cons, "y position value is empty", Snackbar.LENGTH_SHORT).show();
                        return "";
                    }
                    if (input.equals("")) {
                        Snackbar.make(lay_cons, "input text is empty", Snackbar.LENGTH_SHORT).show();
                        return "";
                    }
                    if (input.equals("u")) {
                        commandModel.add(add_list("text", "input tap " + x + " " + y + "\ninput text ", "username", x, y, "", "", input, ""));
                    } else if (input.equals("p")) {
                        commandModel.add(add_list("text", "input tap " + x + " " + y + "\ninput text ", "password", x, y, "", "", input, ""));
                    } else if (input.equals("f")) {
                        commandModel.add(add_list("text", "input tap " + x + " " + y + "\ninput text ", "fullname", x, y, "", "", input, ""));
                    } else if (input.contains("package=")) {
                        commandModel.add(add_list("text", "am start -a android.intent.action.VIEW -d 'market://details?id=" + input.replace("package=", "") + "'", input, x, y, "", "", input, ""));
                    } else if (input.equals("open")) {
                        commandModel.add(add_list("text", "input tap " + x + " " + y, "Open position x= " + x + " y= " + y, x, y, "", "", input, ""));
                    } else {
                        commandModel.add(add_list("text", "input tap " + x + " " + y + "\ninput text '" + input + "'", "click position x= " + x + " x= " + y + " and enter input text '" + input + "'", x, y, "", "", input, ""));
                    }
                }
                if (cm.getType().equals("sleep")) {
                    sec = cm.getSleep().trim();
                    if (sec.equals("")) {
                        Snackbar.make(lay_cons, "sleep second is empty", Snackbar.LENGTH_SHORT).show();
                        return "";
                    }
                    commandModel.add(add_list("sleep", "sleep " + sec, "sleep " + sec + " seconds", "", "", "", "", "", sec));
                }
            }
            check_user(commandModel);
            return new Gson().toJson(commandModel);
        } else {
            return "";
        }
    }

    private CommandModel add_list(String type, String cmd, String desc, String x, String y, String x1, String y1, String text, String sleep) {
        CommandModel cm = new CommandModel();
        cm.setType(type);
        cm.setCommand(cmd);
        cm.setDesc(desc);
        cm.setX(x);
        cm.setY(y);
        cm.setX1(x1);
        cm.setY1(y1);
        cm.setText(text);
        cm.setSleep(sleep);
        return cm;
    }

    private void check_user(List<CommandModel> commandModels) {
        appDB.dataDao().update_no_user("yes", 1);
        for (CommandModel cm : commandModels) {
            if (cm.getDesc().equals("username") || cm.getDesc().equals("password")) {
                appDB.dataDao().update_no_user("no", 1);
            }
        }
    }
}