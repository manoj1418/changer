package com.changer.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Group;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.changer.R;
import com.changer.db.AppDB;
import com.changer.model.CommandModel;
import com.changer.utils.Item_Space_Decoration;
import com.changer.utils.Utils;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AutoActivity extends AppCompatActivity {
    @BindView(R.id.lay_cons)
    ConstraintLayout lay_cons;
    @BindView(R.id.id_toolbar)
    Toolbar id_toolbar;
    @BindView(R.id.ch_click)
    AppCompatCheckBox ch_click;
    @BindView(R.id.ch_swipe)
    AppCompatCheckBox ch_swipe;
    @BindView(R.id.ch_text)
    AppCompatCheckBox ch_text;
    @BindView(R.id.ch_sleep)
    AppCompatCheckBox ch_sleep;
    @BindView(R.id.ed_x)
    AppCompatEditText ed_x;
    @BindView(R.id.ed_y)
    AppCompatEditText ed_y;
    @BindView(R.id.ed_x1)
    AppCompatEditText ed_x1;
    @BindView(R.id.ed_y1)
    AppCompatEditText ed_y1;
    @BindView(R.id.ed_text)
    AppCompatEditText ed_text;
    @BindView(R.id.ed_sec)
    AppCompatEditText ed_sec;
    @BindView(R.id.g_click)
    Group g_click;
    @BindView(R.id.g_swipe)
    Group g_swipe;
    @BindView(R.id.g_sleep)
    Group g_sleep;
    @BindView(R.id.btn_add)
    AppCompatButton btn_add;
    @BindView(R.id.id_rv)
    RecyclerView id_rv;
    private List<CommandModel> commandModel;
    private AutoAdapter adapter;
    private AppDB appDB;
    private String content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auto);
        ButterKnife.bind(this);
        appDB = AppDB.getInstance(this);

        setSupportActionBar(id_toolbar);
        id_toolbar.setNavigationIcon(R.drawable.ic_back);
        id_toolbar.setTitle("Automation");
        id_toolbar.setNavigationOnClickListener(v -> onBackPressed());

        commandModel = new ArrayList<>();
        clear_all(false);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null)
            content = bundle.getString("data");
        if (content == null)
            content = "normal_cmd";

        id_rv.setHasFixedSize(true);
        id_rv.setLayoutManager(new LinearLayoutManager(this));
        id_rv.addItemDecoration(new Item_Space_Decoration(16, 1));
        id_rv.setItemAnimator(new DefaultItemAnimator());
        adapter = new AutoAdapter(commandModel);
        id_rv.setAdapter(adapter);

        ch_click.setOnCheckedChangeListener((buttonView, isChecked) -> {
            clear_all(isChecked);
            ch_click.setChecked(isChecked);
            g_click.setVisibility(isChecked ? View.VISIBLE : View.GONE);
        });

        ch_swipe.setOnCheckedChangeListener((buttonView, isChecked) -> {
            clear_all(isChecked);
            ch_swipe.setChecked(isChecked);
            g_click.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            g_swipe.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            g_sleep.setVisibility(isChecked ? View.VISIBLE : View.GONE);
        });

        ch_text.setOnCheckedChangeListener((buttonView, isChecked) -> {
            clear_all(isChecked);
            ch_text.setChecked(isChecked);
            g_click.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            ed_text.setVisibility(isChecked ? View.VISIBLE : View.GONE);
        });

        ch_sleep.setOnCheckedChangeListener((buttonView, isChecked) -> {
            clear_all(isChecked);
            ch_sleep.setChecked(isChecked);
            g_sleep.setVisibility(isChecked ? View.VISIBLE : View.GONE);
        });
        get_data();
    }

    @OnClick(R.id.btn_add)
    void add_cmd() {
        String x, y, x1, y1, input, sec;
        if (ch_click.isChecked() && !ch_swipe.isChecked() && !ch_text.isChecked() && !ch_sleep.isChecked()) {
            if (Utils.isEmpty(ed_x)) {
                Snackbar.make(lay_cons, "x position value is empty", Snackbar.LENGTH_SHORT).show();
                return;
            }
            if (Utils.isEmpty(ed_y)) {
                Snackbar.make(lay_cons, "y position value is empty", Snackbar.LENGTH_SHORT).show();
                return;
            }
            x = String.valueOf(ed_x.getText()).trim();
            y = String.valueOf(ed_y.getText()).trim();
            add_list("click", "input tap " + x + " " + y, "click position x= " + x + " y= " + y, x, y, "", "", "", "");
        }
        if (!ch_click.isChecked() && ch_swipe.isChecked() && !ch_text.isChecked() && !ch_sleep.isChecked()) {
            if (Utils.isEmpty(ed_x)) {
                Snackbar.make(lay_cons, "x position value is empty", Snackbar.LENGTH_SHORT).show();
                return;
            }
            if (Utils.isEmpty(ed_y)) {
                Snackbar.make(lay_cons, "y position value is empty", Snackbar.LENGTH_SHORT).show();
                return;
            }
            if (Utils.isEmpty(ed_x1)) {
                Snackbar.make(lay_cons, "x1 position value is empty", Snackbar.LENGTH_SHORT).show();
                return;
            }
            if (Utils.isEmpty(ed_y1)) {
                Snackbar.make(lay_cons, "y1 position value is empty", Snackbar.LENGTH_SHORT).show();
                return;
            }
            if (Utils.isEmpty(ed_sec)) {
                Snackbar.make(lay_cons, "duration second is empty", Snackbar.LENGTH_SHORT).show();
                return;
            }
            x = String.valueOf(ed_x.getText()).trim();
            y = String.valueOf(ed_y.getText()).trim();
            x1 = String.valueOf(ed_x1.getText()).trim();
            y1 = String.valueOf(ed_y1.getText()).trim();
            sec = Utils.duration(String.valueOf(ed_sec.getText()).trim());
            add_list("swipe", "input swipe " + x + " " + y + " " + x1 + " " + y1 + " " + sec, "swipe position x= " + x + " y= " + y + " x1= " + x1 + " y1= " + y1 + " duration= " + sec, x, y, x1, y1, "", sec);
        }
        if (!ch_click.isChecked() && !ch_swipe.isChecked() && ch_text.isChecked() && !ch_sleep.isChecked()) {
            if (Utils.isEmpty(ed_x)) {
                Snackbar.make(lay_cons, "x position value is empty", Snackbar.LENGTH_SHORT).show();
                return;
            }
            if (Utils.isEmpty(ed_y)) {
                Snackbar.make(lay_cons, "y position value is empty", Snackbar.LENGTH_SHORT).show();
                return;
            }
            if (Utils.isEmpty(ed_text)) {
                Snackbar.make(lay_cons, "input text is empty", Snackbar.LENGTH_SHORT).show();
                return;
            }
            x = String.valueOf(ed_x.getText()).trim();
            y = String.valueOf(ed_y.getText()).trim();
            input = String.valueOf(ed_text.getText()).toLowerCase().trim();
            if (input.equals("u")) {
                add_list("text", "input tap " + x + " " + y + "\ninput text ", "username", x, y, "", "", input, "");
            } else if (input.equals("p")) {
                add_list("text", "input tap " + x + " " + y + "\ninput text ", "password", x, y, "", "", input, "");
            } else if (input.equals("f")) {
                add_list("text", "input tap " + x + " " + y + "\ninput text ", "fullname", x, y, "", "", input, "");
            } else if (input.contains("package=")) {
                add_list("text", "am start -a android.intent.action.VIEW -d \'market://details?id=" + input.replace("package=", "") + "\'", input, x, y, "", "", input, "");
            } else if (input.equals("open")) {
                add_list("text", "input tap " + x + " " + y, "Open position x= " + x + " y= " + y, x, y, "", "", input, "");
            } else {
                add_list("text", "input tap " + x + " " + y + "\ninput text \'" + input + "\'", "click position x= " + x + " x= " + y + " and enter input text \'" + input + "\'", x, y, "", "", input, "");
            }
        }
        if (!ch_click.isChecked() && !ch_swipe.isChecked() && !ch_text.isChecked() && ch_sleep.isChecked()) {
            if (Utils.isEmpty(ed_sec)) {
                Snackbar.make(lay_cons, "sleep second is empty", Snackbar.LENGTH_SHORT).show();
                return;
            }
            sec = String.valueOf(ed_sec.getText()).trim();
            add_list("sleep", "sleep " + sec, "sleep " + sec + " seconds", "", "", "", "", "", sec);
        }
    }

    private void add_list(String type, String cmd, String desc, String x, String y, String x1, String y1, String text, String sleep) {
        CommandModel cm = new CommandModel();
        cm.setType(type);
        cm.setCommand(cmd);
        cm.setDesc(desc);
        cm.setX(x);
        cm.setY(y);
        cm.setX1(x1);
        cm.setY1(y1);
        cm.setText(text);
        cm.setSleep(sleep);
        commandModel.add(cm);
        adapter.notifyDataSetChanged();
        check_user(commandModel);
        if (content.equals("normal_cmd")) {
            switch (appDB.dataDao().get_install_type(1)) {
                case "playstore":
                    appDB.dataDao().update_playstore_cmd(new Gson().toJson(commandModel), 1);
                    break;
                case "custom":
                    appDB.dataDao().update_url_cmd(new Gson().toJson(commandModel), 1);
                    break;
            }
        } else if (content.equals("action_cmd")) {
            appDB.dataDao().update_a_url_cmd(new Gson().toJson(commandModel), 1);
        }
    }

    private void get_data() {
        if (content != null) {
            String s_cmd;
            if (content.equals("normal_cmd")) {
                if (appDB.dataDao().get_install_type(1) != null) {
                    switch (appDB.dataDao().get_install_type(1)) {
                        case "playstore":
                            s_cmd = appDB.dataDao().get_playstore_cmd(1);
                            if (s_cmd != null) {
                                commandModel.clear();
                                commandModel.addAll(new Gson().fromJson(s_cmd, new TypeToken<List<CommandModel>>() {
                                }.getType()));
                            }
                            id_toolbar.setSubtitle("Automation type - Playstore");
                            break;
                        case "custom":
                            s_cmd = appDB.dataDao().get_url_cmd(1);
                            if (s_cmd != null) {
                                commandModel.clear();
                                commandModel.addAll(new Gson().fromJson(s_cmd, new TypeToken<List<CommandModel>>() {
                                }.getType()));
                            }
                            id_toolbar.setSubtitle("Automation type - Custom Url");
                            break;
                    }
                }
            } else if (content.equals("action_cmd")) {
                if (appDB.dataDao().get_element_type(1) != null) {
                    if (appDB.dataDao().get_element_type(1).equals("automation")) {
                        s_cmd = appDB.dataDao().get_a_url_cmd(1);
                        if (s_cmd != null) {
                            commandModel.clear();
                            commandModel.addAll(new Gson().fromJson(s_cmd, new TypeToken<List<CommandModel>>() {
                            }.getType()));
                        }
                        id_toolbar.setSubtitle("Automation type - Download Action");
                    }
                }
            }
            adapter.notifyDataSetChanged();
        }
    }

    private void clear_all(Boolean b) {
        ch_click.setChecked(false);
        ch_swipe.setChecked(false);
        ch_text.setChecked(false);
        ch_sleep.setChecked(false);
        g_click.setVisibility(View.GONE);
        ed_text.setVisibility(View.GONE);
        g_swipe.setVisibility(View.GONE);
        g_sleep.setVisibility(View.GONE);
        btn_add.setVisibility(b ? View.VISIBLE : View.GONE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.auto_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (R.id.s_edit == item.getItemId()) {
            Intent intent = new Intent(AutoActivity.this, EditActivity.class);
            intent.putExtra("data", "normal_cmd");
            startActivity(intent);
            return true;
        } else if (R.id.s_refresh == item.getItemId()) {
            get_data();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        get_data();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void check_user(List<CommandModel> commandModels) {
        appDB.dataDao().update_no_user("yes", 1);
        for (CommandModel cm : commandModels) {
            if (cm.getDesc().equals("username") || cm.getDesc().equals("password")) {
                appDB.dataDao().update_no_user("no", 1);
            }
        }
    }
}