package com.changer.activity;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.changer.R;
import com.changer.model.CampaignModel;
import com.changer.utils.M_Listener;

import java.util.List;

public class CampaignAdapter extends RecyclerView.Adapter<CampaignAdapter.ViewHolder> {
    private List<CampaignModel> campaignModels;
    private M_Listener listener;

    public CampaignAdapter(List<CampaignModel> campaignModels, M_Listener listener) {
        this.campaignModels = campaignModels;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ConstraintLayout cl = (ConstraintLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.cv_command, parent, false);
        return new ViewHolder(cl);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.tx_command.setText(campaignModels.get(position).getCamp_name());
        holder.tx_command.setOnClickListener(v -> {
            if (listener != null)
                listener.Click(position);
        });
    }

    @Override
    public int getItemCount() {
        return campaignModels.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tx_command;

        ViewHolder(@NonNull ConstraintLayout itemView) {
            super(itemView);
            tx_command = itemView.findViewById(R.id.tx_command);
        }
    }
}