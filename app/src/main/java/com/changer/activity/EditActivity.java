package com.changer.activity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Group;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.changer.R;
import com.changer.db.AppDB;
import com.changer.model.CommandModel;
import com.changer.utils.Drag_Swipe;
import com.changer.utils.Item_Space_Decoration;
import com.changer.utils.Touch_Listener;
import com.changer.utils.Utils;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EditActivity extends AppCompatActivity implements Touch_Listener {
    @BindView(R.id.lay_cons)
    ConstraintLayout lay_cons;
    @BindView(R.id.id_toolbar)
    Toolbar id_toolbar;
    @BindView(R.id.id_rv)
    RecyclerView id_rv;
    private List<CommandModel> commandModel;
    private CommandModel cm;
    private AutoAdapter adapter;
    private AppDB appDB;
    private String content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        ButterKnife.bind(this);
        appDB = AppDB.getInstance(this);

        setSupportActionBar(id_toolbar);
        id_toolbar.setNavigationIcon(R.drawable.ic_back);
        id_toolbar.setTitle("Edit Commands");
        id_toolbar.setNavigationOnClickListener(v -> onBackPressed());

        commandModel = new ArrayList<>();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null)
            content = bundle.getString("data");
        if (content == null)
            content = "normal_cmd";

        id_rv.setHasFixedSize(true);
        id_rv.setLayoutManager(new LinearLayoutManager(this));
        Item_Space_Decoration item_space_decoration = new Item_Space_Decoration(16, 1);
        id_rv.addItemDecoration(item_space_decoration);
        adapter = new AutoAdapter(commandModel, this);
        id_rv.setAdapter(adapter);
        ItemTouchHelper.Callback callback = new Drag_Swipe(this);
        new ItemTouchHelper(callback).attachToRecyclerView(id_rv);
        get_data();
    }

    private void get_data() {
        if (content != null) {
            String s_cmd;
            if (content.equals("normal_cmd")) {
                if (appDB.dataDao().get_install_type(1) != null) {
                    switch (appDB.dataDao().get_install_type(1)) {
                        case "playstore":
                            s_cmd = appDB.dataDao().get_playstore_cmd(1);
                            if (s_cmd != null) {
                                commandModel.clear();
                                commandModel.addAll(new Gson().fromJson(s_cmd, new TypeToken<List<CommandModel>>() {
                                }.getType()));
                            }
                            id_toolbar.setSubtitle("Automation type - Playstore");
                            break;
                        case "custom":
                            s_cmd = appDB.dataDao().get_url_cmd(1);
                            if (s_cmd != null) {
                                commandModel.clear();
                                commandModel.addAll(new Gson().fromJson(s_cmd, new TypeToken<List<CommandModel>>() {
                                }.getType()));
                            }
                            id_toolbar.setSubtitle("Automation type - Custom Url");
                            break;
                    }
                }
            } else if (content.equals("action_cmd")) {
                if (appDB.dataDao().get_element_type(1) != null) {
                    if (appDB.dataDao().get_element_type(1).equals("automation")) {
                        s_cmd = appDB.dataDao().get_a_url_cmd(1);
                        if (s_cmd != null) {
                            commandModel.clear();
                            commandModel.addAll(new Gson().fromJson(s_cmd, new TypeToken<List<CommandModel>>() {
                            }.getType()));
                        }
                        id_toolbar.setSubtitle("Automation type - Download Action");
                    }
                }
            }
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (R.id.s_refresh == item.getItemId()) {
            get_data();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void Drag(int f_position, int t_position) {
        if (f_position < t_position) {
            for (int i = f_position; i < t_position; i++) {
                Collections.swap(commandModel, i, i + 1);
            }
        } else {
            for (int i = f_position; i > t_position; i--) {
                Collections.swap(commandModel, i, i - 1);
            }
        }
        adapter.notifyItemMoved(f_position, t_position);
        check_user(commandModel);
        if (content.equals("normal_cmd")) {
            switch (appDB.dataDao().get_install_type(1)) {
                case "playstore":
                    appDB.dataDao().update_playstore_cmd(new Gson().toJson(commandModel), 1);
                    break;
                case "custom":
                    appDB.dataDao().update_url_cmd(new Gson().toJson(commandModel), 1);
                    break;
            }
        } else if (content.equals("action_cmd")) {
            appDB.dataDao().update_a_url_cmd(new Gson().toJson(commandModel), 1);
        }
    }

    @Override
    public void Swipe(int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to delete ?")
                .setCancelable(false)
                .setPositiveButton("Yes", (dialog, id) -> {
                    commandModel.remove(position);
                    adapter.notifyItemRemoved(position);
                    check_user(commandModel);
                    if (content.equals("normal_cmd")) {
                        switch (appDB.dataDao().get_install_type(1)) {
                            case "playstore":
                                appDB.dataDao().update_playstore_cmd(new Gson().toJson(commandModel), 1);
                                break;
                            case "custom":
                                appDB.dataDao().update_url_cmd(new Gson().toJson(commandModel), 1);
                                break;
                        }
                    } else if (content.equals("action_cmd")) {
                        appDB.dataDao().update_a_url_cmd(new Gson().toJson(commandModel), 1);
                    }
                })
                .setNegativeButton("No", (dialog, id) ->
                {
                    adapter.notifyDataSetChanged();
                    dialog.dismiss();
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void Edit(int position) {
        cm = commandModel.get(position);
        if (cm != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(false);
            AlertDialog alert = builder.create();
            final View view = getLayoutInflater().inflate(R.layout.lay_edit, null);

            final AppCompatCheckBox ch_click = view.findViewById(R.id.ch_click);
            final AppCompatCheckBox ch_text = view.findViewById(R.id.ch_text);
            final AppCompatCheckBox ch_sleep = view.findViewById(R.id.ch_sleep);
            final AppCompatEditText ed_x = view.findViewById(R.id.ed_x);
            final AppCompatEditText ed_y = view.findViewById(R.id.ed_y);
            final AppCompatEditText ed_text = view.findViewById(R.id.ed_text);
            final AppCompatEditText ed_sec = view.findViewById(R.id.ed_sec);
            final Group g_click = view.findViewById(R.id.g_click);
            final Group g_sleep = view.findViewById(R.id.g_sleep);
            final AppCompatButton btn_update = view.findViewById(R.id.btn_update);
            final AppCompatButton btn_cancel = view.findViewById(R.id.btn_cancel);

            ch_click.setOnCheckedChangeListener((buttonView, isChecked) -> {
                ch_click.setChecked(false);
                ch_text.setChecked(false);
                ch_sleep.setChecked(false);
                g_click.setVisibility(View.GONE);
                ed_text.setVisibility(View.GONE);
                g_sleep.setVisibility(View.GONE);
                btn_update.setVisibility(isChecked ? View.VISIBLE : View.GONE);
                ch_click.setChecked(isChecked);
                g_click.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            });

            ch_text.setOnCheckedChangeListener((buttonView, isChecked) -> {
                ch_click.setChecked(false);
                ch_text.setChecked(false);
                ch_sleep.setChecked(false);
                g_click.setVisibility(View.GONE);
                ed_text.setVisibility(View.GONE);
                g_sleep.setVisibility(View.GONE);
                btn_update.setVisibility(isChecked ? View.VISIBLE : View.GONE);
                ch_text.setChecked(isChecked);
                g_click.setVisibility(isChecked ? View.VISIBLE : View.GONE);
                ed_text.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            });

            ch_sleep.setOnCheckedChangeListener((buttonView, isChecked) -> {
                ch_click.setChecked(false);
                ch_text.setChecked(false);
                ch_sleep.setChecked(false);
                g_click.setVisibility(View.GONE);
                ed_text.setVisibility(View.GONE);
                g_sleep.setVisibility(View.GONE);
                btn_update.setVisibility(isChecked ? View.VISIBLE : View.GONE);
                ch_sleep.setChecked(isChecked);
                g_sleep.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            });

            if (cm.getType() != null) {
                switch (cm.getType()) {
                    case "click":
                        ch_click.setChecked(true);
                        ed_x.setText(cm.getX());
                        ed_y.setText(cm.getY());
                        ed_text.setText(cm.getText());
                        ed_sec.setText(cm.getSleep());
                        break;
                    case "text":
                        ch_text.setChecked(true);
                        ed_x.setText(cm.getX());
                        ed_y.setText(cm.getY());
                        ed_text.setText(cm.getText());
                        ed_sec.setText(cm.getSleep());
                        break;
                    case "sleep":
                        ch_sleep.setChecked(true);
                        ed_x.setText(cm.getX());
                        ed_y.setText(cm.getY());
                        ed_text.setText(cm.getText());
                        ed_sec.setText(cm.getSleep());
                        break;
                }
            }

            btn_update.setOnClickListener(v -> {
                String x, y, input, sec;
                if (ch_click.isChecked() && !ch_text.isChecked() && !ch_sleep.isChecked()) {
                    if (Utils.isEmpty(ed_x)) {
                        Snackbar.make(lay_cons, "x position value is empty", Snackbar.LENGTH_SHORT).show();
                        return;
                    }
                    if (Utils.isEmpty(ed_y)) {
                        Snackbar.make(lay_cons, "y position value is empty", Snackbar.LENGTH_SHORT).show();
                        return;
                    }
                    x = String.valueOf(ed_x.getText()).trim();
                    y = String.valueOf(ed_y.getText()).trim();
                    add_list(position, "click", "input tap " + x + " " + y, "click position x= " + x + " y= " + y, x, y, "", "");
                }
                if (!ch_click.isChecked() && ch_text.isChecked() && !ch_sleep.isChecked()) {
                    if (Utils.isEmpty(ed_x)) {
                        Snackbar.make(lay_cons, "x position value is empty", Snackbar.LENGTH_SHORT).show();
                        return;
                    }
                    if (Utils.isEmpty(ed_y)) {
                        Snackbar.make(lay_cons, "y position value is empty", Snackbar.LENGTH_SHORT).show();
                        return;
                    }
                    if (Utils.isEmpty(ed_text)) {
                        Snackbar.make(lay_cons, "input text is empty", Snackbar.LENGTH_SHORT).show();
                        return;
                    }
                    x = String.valueOf(ed_x.getText()).trim();
                    y = String.valueOf(ed_y.getText()).trim();
                    input = String.valueOf(ed_text.getText()).toLowerCase().trim();
                    if (input.equals("u")) {
                        add_list(position, "text", "input tap " + x + " " + y + "\ninput text ", "username", x, y, input, "");
                    } else if (input.equals("p")) {
                        add_list(position, "text", "input tap " + x + " " + y + "\ninput text ", "password", x, y, input, "");
                    } else if (input.equals("f")) {
                        add_list(position, "text", "input tap " + x + " " + y + "\ninput text ", "fullname", x, y, input, "");
                    } else if (input.contains("package=")) {
                        add_list(position, "text", "am start -a android.intent.action.VIEW -d \'market://details?id=" + input.replace("package=", "") + "\'", input, x, y, input, "");
                    } else if (input.equals("open")) {
                        add_list(position, "text", "input tap " + x + " " + y, "Open position x= " + x + " y= " + y, x, y, input, "");
                    } else {
                        add_list(position, "text", "input tap " + x + " " + y + "\ninput text \'" + input + "\'", "click position x= " + x + " x= " + y + " and enter input text \'" + input + "\'", x, y, input, "");
                    }
                }
                if (!ch_click.isChecked() && !ch_text.isChecked() && ch_sleep.isChecked()) {
                    if (Utils.isEmpty(ed_sec)) {
                        Snackbar.make(lay_cons, "sleep second is empty", Snackbar.LENGTH_SHORT).show();
                        return;
                    }
                    sec = String.valueOf(ed_sec.getText()).trim();
                    add_list(position, "sleep", "sleep " + sec, "sleep " + sec + " seconds", "", "", "", sec);
                }
                alert.dismiss();
            });
            btn_cancel.setOnClickListener(v -> alert.dismiss());
            alert.setView(view);
            alert.show();
        }
    }

    private void add_list(int position, String type, String cmd, String desc, String x, String y, String text, String sleep) {
        cm = new CommandModel();
        cm.setType(type);
        cm.setCommand(cmd);
        cm.setDesc(desc);
        cm.setX(x);
        cm.setY(y);
        cm.setText(text);
        cm.setSleep(sleep);
        commandModel.remove(position);
        commandModel.add(position, cm);
        adapter.notifyDataSetChanged();
        check_user(commandModel);
        if (content.equals("normal_cmd")) {
            switch (appDB.dataDao().get_install_type(1)) {
                case "playstore":
                    appDB.dataDao().update_playstore_cmd(new Gson().toJson(commandModel), 1);
                    break;
                case "custom":
                    appDB.dataDao().update_url_cmd(new Gson().toJson(commandModel), 1);
                    break;
            }
        } else if (content.equals("action_cmd")) {
            appDB.dataDao().update_a_url_cmd(new Gson().toJson(commandModel), 1);
        }
    }

    private void check_user(List<CommandModel> commandModels) {
        appDB.dataDao().update_no_user("yes", 1);
        for (CommandModel cm : commandModels) {
            if (cm.getDesc().equals("username") || cm.getDesc().equals("password")) {
                appDB.dataDao().update_no_user("no", 1);
            }
        }
    }
}