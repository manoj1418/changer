package com.changer.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.changer.R;
import com.changer.db.AppDB;
import com.changer.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SettingsActivity extends AppCompatActivity {
    @BindView(R.id.lay_cons)
    ConstraintLayout lay_cons;
    @BindView(R.id.id_toolbar)
    Toolbar id_toolbar;
    @BindView(R.id.ch_automation)
    AppCompatCheckBox ch_automation;
    @BindView(R.id.ch_playstore)
    AppCompatCheckBox ch_playstore;
    @BindView(R.id.ch_priv_url)
    AppCompatCheckBox ch_priv_url;
    @BindView(R.id.tx_automation)
    AppCompatTextView tx_automation;
    @BindView(R.id.tx_priv_url)
    AppCompatTextView tx_priv_url;
    private AppDB appDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);
        appDB = AppDB.getInstance(this);

        id_toolbar.setNavigationIcon(R.drawable.ic_back);
        id_toolbar.setTitle("Settings");
        id_toolbar.setNavigationOnClickListener(v -> onBackPressed());

        if (appDB.dataDao().get_automation(1) != null) {
            if (appDB.dataDao().get_automation(1).equals("on")) {
                ch_automation.setChecked(true);
                tx_automation.setVisibility(View.VISIBLE);
            } else if (appDB.dataDao().get_automation(1).equals("off")) {
                ch_automation.setChecked(false);
                tx_automation.setVisibility(View.GONE);
            }
        }

        if (appDB.dataDao().get_install_type(1) != null) {
            if (appDB.dataDao().get_install_type(1).equals("playstore")) {
                ch_playstore.setChecked(true);
                ch_priv_url.setChecked(false);
                tx_priv_url.setVisibility(View.GONE);
                id_toolbar.setSubtitle("Install type - Playstore");
            } else if (appDB.dataDao().get_install_type(1).equals("custom")) {
                ch_playstore.setChecked(false);
                ch_priv_url.setChecked(true);
                tx_priv_url.setVisibility(View.VISIBLE);
                id_toolbar.setSubtitle("Install type - Custom Url");
            }
        }

        ch_automation.setOnCheckedChangeListener((buttonView, isChecked) -> {
            tx_automation.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            appDB.dataDao().set_automation(isChecked ? "on" : "off", 1);
        });

        ch_playstore.setOnCheckedChangeListener((buttonView, isChecked) -> {
            ch_priv_url.setChecked(!isChecked);
            tx_priv_url.setVisibility(isChecked ? View.GONE : View.VISIBLE);
            id_toolbar.setSubtitle("");
            if (isChecked)
                appDB.dataDao().update_install_type("playstore", 1);
            id_toolbar.setSubtitle(isChecked ? "Install type - Playstore" : "Install type - Custom Url");
        });

        ch_priv_url.setOnCheckedChangeListener((buttonView, isChecked) -> {
            ch_playstore.setChecked(!isChecked);
            tx_priv_url.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            id_toolbar.setSubtitle("");
            if (isChecked)
                appDB.dataDao().update_install_type("custom", 1);
            id_toolbar.setSubtitle(isChecked ? "Install type - Custom Url" : "Install type - Playstore");
        });
    }

    @OnClick(R.id.tx_automation)
    void automation() {
        Intent intent = new Intent(SettingsActivity.this, AutoActivity.class);
        intent.putExtra("data", "normal_cmd");
        startActivity(intent);
    }

    @OnClick(R.id.tx_priv_url)
    void private_url() {
        startActivity(new Intent(SettingsActivity.this, CustomUrlActivity.class));
    }

    @OnClick(R.id.tx_d_campaign)
    void open_device_list() {
        startActivity(new Intent(SettingsActivity.this, DeviceActivity.class));
    }

    @OnClick(R.id.tx_launcher)
    void set_launcher() {
        getPackageManager().clearPackagePreferredActivities("com.changer");
        startActivity(new Intent(Intent.ACTION_MAIN).addCategory(Intent.CATEGORY_HOME).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    @OnClick(R.id.tx_backup)
    void backup() {
        Utils.backup(this, lay_cons);
    }

    @OnClick(R.id.tx_restore)
    void restore() {
        Utils.restore(this, lay_cons);
    }

    @OnClick(R.id.tx_c_install_count)
    void clear_install_count() {
        appDB.dataDao().update_install_count("0", 1);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}