package com.changer.activity;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.changer.R;
import com.changer.model.CommandModel;
import com.changer.utils.Touch_Listener;

import java.util.List;

public class AutoAdapter extends RecyclerView.Adapter<AutoAdapter.ViewHolder> {
    private List<CommandModel> commandModel;
    private Touch_Listener listener;

    public AutoAdapter(List<CommandModel> commandModel) {
        this.commandModel = commandModel;
    }

    public AutoAdapter(List<CommandModel> commandModel, Touch_Listener listener) {
        this.commandModel = commandModel;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ConstraintLayout cl = (ConstraintLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.cv_command, parent, false);
        return new ViewHolder(cl);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.tx_command.setText(commandModel.get(position).getDesc());
        holder.tx_command.setOnClickListener(v -> {
            if (listener != null)
                listener.Edit(position);
        });
    }

    @Override
    public int getItemCount() {
        return commandModel.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tx_command;

        ViewHolder(@NonNull ConstraintLayout itemView) {
            super(itemView);
            tx_command = itemView.findViewById(R.id.tx_command);
        }
    }
}